/**
 * MCFirstViewServiceImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.firstView.services.addcel.com;

import com.addcel.ingo.bridge.client.model.vo.ServicesEndPoint;

public class MCFirstViewServiceImplLocator extends org.apache.axis.client.Service implements ws.firstView.services.addcel.com.MCFirstViewServiceImpl {

    public MCFirstViewServiceImplLocator() {
    }


    public MCFirstViewServiceImplLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MCFirstViewServiceImplLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MCFirstViewBridgeServicePort
    private java.lang.String MCFirstViewBridgeServicePort_address = ServicesEndPoint.FirstViewWs;//"http://199.231.161.38:8081/FirstViewBridgeWS/Services";

    public java.lang.String getMCFirstViewBridgeServicePortAddress() {
        return MCFirstViewBridgeServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MCFirstViewBridgeServicePortWSDDServiceName = "MCFirstViewBridgeServicePort";

    public java.lang.String getMCFirstViewBridgeServicePortWSDDServiceName() {
        return MCFirstViewBridgeServicePortWSDDServiceName;
    }

    public void setMCFirstViewBridgeServicePortWSDDServiceName(java.lang.String name) {
        MCFirstViewBridgeServicePortWSDDServiceName = name;
    }

    public com.addcel.firstView.service.Services getMCFirstViewBridgeServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MCFirstViewBridgeServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMCFirstViewBridgeServicePort(endpoint);
    }

    public com.addcel.firstView.service.Services getMCFirstViewBridgeServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.firstView.services.addcel.com.MCFirstViewServiceImplSoapBindingStub _stub = new ws.firstView.services.addcel.com.MCFirstViewServiceImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getMCFirstViewBridgeServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMCFirstViewBridgeServicePortEndpointAddress(java.lang.String address) {
        MCFirstViewBridgeServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.firstView.service.Services.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.firstView.services.addcel.com.MCFirstViewServiceImplSoapBindingStub _stub = new ws.firstView.services.addcel.com.MCFirstViewServiceImplSoapBindingStub(new java.net.URL(MCFirstViewBridgeServicePort_address), this);
                _stub.setPortName(getMCFirstViewBridgeServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MCFirstViewBridgeServicePort".equals(inputPortName)) {
            return getMCFirstViewBridgeServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://com.addcel.services.firstView.ws", "MCFirstViewServiceImpl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://com.addcel.services.firstView.ws", "MCFirstViewBridgeServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MCFirstViewBridgeServicePort".equals(portName)) {
            setMCFirstViewBridgeServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
