/**
 * MCFirstViewServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws.firstView.services.addcel.com;

public interface MCFirstViewServiceImpl extends javax.xml.rpc.Service {
    public java.lang.String getMCFirstViewBridgeServicePortAddress();

    public com.addcel.firstView.service.Services getMCFirstViewBridgeServicePort() throws javax.xml.rpc.ServiceException;

    public com.addcel.firstView.service.Services getMCFirstViewBridgeServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
