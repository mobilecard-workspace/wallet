package com.addcel.ingo.bridge.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnrollCustomerMC extends EnrollCustomerRequest{

	private String customerId;
	private String cardNumber;
	private String expirationMonthYear; 
	private String cardNickname; 
	private String nameOnCard; 
	private String deviceId;
	
	public EnrollCustomerMC() {
		// TODO Auto-generated constructor stub
	}

	
	public String getDeviceId() {
		return deviceId;
	}


	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}


	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpirationMonthYear() {
		return expirationMonthYear;
	}

	public void setExpirationMonthYear(String expirationMonthYear) {
		this.expirationMonthYear = expirationMonthYear;
	}

	public String getCardNickname() {
		return cardNickname;
	}

	public void setCardNickname(String cardNickname) {
		this.cardNickname = cardNickname;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	

}
