package com.addcel.ingo.bridge.client.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticateOBORequest {

	private String customerId;

	public AuthenticateOBORequest() {
		// TODO Auto-generated constructor stub
	}
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	
	
}
