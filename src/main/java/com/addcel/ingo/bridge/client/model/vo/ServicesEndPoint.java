package com.addcel.ingo.bridge.client.model.vo;

public class ServicesEndPoint {

	//private static final String endPoint = "http://192.168.75.53:80/MCIngo/"; //QA
	private static final String endPoint = "http://localhost:80/MCIngo/"; //PROD
	
	public static final String getSession = endPoint + "getSession"; //192.168.75.53
	public static final String FindCustomer = endPoint + "FindCustomer";
	public static final String GetRegisteredCards = endPoint + "GetRegisteredCards";
	public static final String EnrollCustomer = endPoint + "EnrollCustomer";
	public static final String AddCustomerAttributes = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddCustomerAttributes";
	public static final String AddOrUpdateCard = endPoint + "AddOrUpdateCard";
	public static final String AddOrUpdateTokenizedCard  = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddOrUpdateTokenizedCard";
	public static final String DeleteCard = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/DeleteCard";
	public static final String AuthenticateOBO = endPoint + "AuthenticateOBO";
	public static final String AddSessionAttributes  = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/AddSessionAttributes";
	public static final String UpdateAccount = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/UpdateAccount";
	public static final String GetTransactionHistory = endPoint + "SpykeMobileServices4.0/IngoSDKSupportAPIService.svc/json/GetTransactionHistory";
	
	//public static final String FirstViewWs = "http://192.168.75.53:80/FirstViewBridgeWS/Services"; //QA
	public static final String FirstViewWs = "http://192.168.75.51:80/FirstViewBridgeWS/Services"; // PROD
	
	
//	public static final String ViamericascreateSenderProfileCard = "http://192.168.75.53:80/MCTransfersViamericas/transfers/createSenderProfileCard"; // QA
	public static final String ViamericascreateSenderProfileCard = "http://192.168.75.51:80/MCTransfersViamericas/transfers/createSenderProfileCard"; // PROD
	
	/**
	 * {
"partnerConnectId":"Api.ConnectId.Android.050516.192907@chexar.com",
"partnerConnectToken":"YzAxNzBkMGYtZTNjOS00YjM2LTg2ZjUtOTNkMWFkOWI2MmZk"
}
	 */
	
}
