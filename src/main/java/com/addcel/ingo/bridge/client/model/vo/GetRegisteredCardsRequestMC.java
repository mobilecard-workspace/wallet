package com.addcel.ingo.bridge.client.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRegisteredCardsRequestMC extends GetRegisteredCardsRequest {

	private String deviceId;

	public GetRegisteredCardsRequestMC() {
		// TODO Auto-generated constructor stub
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
