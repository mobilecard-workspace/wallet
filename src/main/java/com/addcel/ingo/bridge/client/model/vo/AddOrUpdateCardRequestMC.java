package com.addcel.ingo.bridge.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddOrUpdateCardRequestMC extends AddOrUpdateCardRequest {

	private String diviceId;
	
	public AddOrUpdateCardRequestMC() {
		// TODO Auto-generated constructor stub
	}

	public String getDiviceId() {
		return diviceId;
	}

	public void setDiviceId(String diviceId) {
		this.diviceId = diviceId;
	}
	
	
}
