package com.addcel.firstView.service;

public class ServicesProxy implements com.addcel.firstView.service.Services {
  private String _endpoint = null;
  private com.addcel.firstView.service.Services services = null;
  
  public ServicesProxy() {
    _initServicesProxy();
  }
  
  public ServicesProxy(String endpoint) {
    _endpoint = endpoint;
    _initServicesProxy();
  }
  
  private void _initServicesProxy() {
    try {
      services = (new ws.firstView.services.addcel.com.MCFirstViewServiceImplLocator()).getMCFirstViewBridgeServicePort();
      if (services != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)services)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)services)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (services != null)
      ((javax.xml.rpc.Stub)services)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.firstView.service.Services getServices() {
    if (services == null)
      _initServicesProxy();
    return services;
  }
  
  public java.lang.String getInfoUser(java.lang.String json) throws java.rmi.RemoteException{
    if (services == null)
      _initServicesProxy();
    return services.getInfoUser(json);
  }
  
  public java.lang.String transactionHistory(java.lang.String json) throws java.rmi.RemoteException{
    if (services == null)
      _initServicesProxy();
    return services.transactionHistory(json);
  }
  
  public java.lang.String createCard(java.lang.String json) throws java.rmi.RemoteException{
    if (services == null)
      _initServicesProxy();
    return services.createCard(json);
  }
  
  public java.lang.String cardUpdate(java.lang.String json) throws java.rmi.RemoteException{
    if (services == null)
      _initServicesProxy();
    return services.cardUpdate(json);
  }
  
  public java.lang.String checkBalance(java.lang.String json) throws java.rmi.RemoteException{
    if (services == null)
      _initServicesProxy();
    return services.checkBalance(json);
  }
  
  
}