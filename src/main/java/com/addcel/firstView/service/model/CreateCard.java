package com.addcel.firstView.service.model;

public class CreateCard {

	private long idUsuario;
	
	private String name;
	
	private String patern;
	
	private String address;
	
	private String state;
	
	private String city;
	
	private String postalCode;
	
	private String country;
	
	private String email;
	
	private String phone;
	
	private String birthday;
	
	private String ssn;
	
	private String productType;
	
	private String subProductType;
	
	private String accountNumber;
	
	private String cardNumber;
	
	private String ddaNo;
	
	private String pan;
	
	private String referenceNo;
	
	private String currencyCode;
	
	private String responseCode;
	
	private String reasonDescription;
	
	private String govtId;
	private String govtIdIssueDate; //(02072014)MMDDYYY
	private String govtIdExpirationDate; // (03012020)MMDDYYYY
	private String govtIdIssueState;
	
	

	public String getGovtId() {
		return govtId;
	}

	public void setGovtId(String govtId) {
		this.govtId = govtId;
	}

	public String getGovtIdIssueDate() {
		return govtIdIssueDate;
	}

	public void setGovtIdIssueDate(String govtIdIssueDate) {
		this.govtIdIssueDate = govtIdIssueDate;
	}

	public String getGovtIdExpirationDate() {
		return govtIdExpirationDate;
	}

	public void setGovtIdExpirationDate(String govtIdExpirationDate) {
		this.govtIdExpirationDate = govtIdExpirationDate;
	}

	public String getGovtIdIssueState() {
		return govtIdIssueState;
	}

	public void setGovtIdIssueState(String govtIdIssueState) {
		this.govtIdIssueState = govtIdIssueState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatern() {
		return patern;
	}

	public void setPatern(String patern) {
		this.patern = patern;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSubProductType() {
		return subProductType;
	}

	public void setSubProductType(String subProductType) {
		this.subProductType = subProductType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDdaNo() {
		return ddaNo;
	}

	public void setDdaNo(String ddaNo) {
		this.ddaNo = ddaNo;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
}
