/**
 * Services.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.firstView.service;

public interface Services extends java.rmi.Remote {
    public java.lang.String getInfoUser(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String transactionHistory(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String createCard(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String cardUpdate(java.lang.String json) throws java.rmi.RemoteException;
    public java.lang.String checkBalance(java.lang.String json) throws java.rmi.RemoteException;
}
