package com.addcel.wallet.mybatis.model.vo;

public class Estados {
	

	private String nombre;
	private int pais;
	private String abreviatura;
	private int id;
	
	public Estados() {
		// TODO Auto-generated constructor stub
	}

	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPais() {
		return pais;
	}

	public void setPais(int pais) {
		this.pais = pais;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	
	

}
