package com.addcel.wallet.mybatis.model.vo;

public class PrevivaleCard {

	private long idUsuario;
	
	private int idTarjeta;

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	
}
