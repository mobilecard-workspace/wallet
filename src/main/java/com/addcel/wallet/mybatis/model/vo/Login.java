package com.addcel.wallet.mybatis.model.vo;

public class Login {

	private String user;
	private String pass;
	private String id_sender;
	private int idpais;
	
	public Login() {
		// TODO Auto-generated constructor stub
	}
	
	public void setIdpais(int idpais) {
		this.idpais = idpais;
	}
	
	public int getIdpais() {
		return idpais;
	}
	
	public void setId_sender(String id_sender) {
		this.id_sender = id_sender;
	}
	
	public String getId_sender() {
		return id_sender;
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	
	
}
