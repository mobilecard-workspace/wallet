package com.addcel.wallet.mybatis.model.vo;

public class CardImage {

	private String url_imagen_corta;
	private String url_imagen_larga;
	
	public CardImage() {
		// TODO Auto-generated constructor stub
	}

	public String getUrl_imagen_corta() {
		return url_imagen_corta;
	}

	public void setUrl_imagen_corta(String url_imagen_corta) {
		this.url_imagen_corta = url_imagen_corta;
	}

	public String getUrl_imagen_larga() {
		return url_imagen_larga;
	}

	public void setUrl_imagen_larga(String url_imagen_larga) {
		this.url_imagen_larga = url_imagen_larga;
	}
	
	
	
	
}
