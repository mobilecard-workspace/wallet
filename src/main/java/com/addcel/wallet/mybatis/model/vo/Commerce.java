package com.addcel.wallet.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Commerce {

	private long id;
	private String municipio;
	private String colonia;
	private String cp;
	private long id_estado;
	private String calle;  
	private Integer id_aplicacion;
	
	public Commerce() {
		// TODO Auto-generated constructor stub
	}

	public void setId_aplicacion(Integer id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}
	
	public Integer getId_aplicacion() {
		return id_aplicacion;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public long getId_estado() {
		return id_estado;
	}

	public void setId_estado(long id_estado) {
		this.id_estado = id_estado;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	

}
