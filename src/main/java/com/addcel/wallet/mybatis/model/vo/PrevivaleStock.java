package com.addcel.wallet.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PrevivaleStock {
	
	private Long id_previvale_card_stock;
	private String numero; 
	private String vigencia;
	private String ct;
	private long idtarjetas_establecimiento;
	private String id_stp_cuentas_clabe;
	
	public PrevivaleStock() {
		// TODO Auto-generated constructor stub
	}

	public void setIdtarjetas_establecimiento(long idtarjetas_establecimiento) {
		this.idtarjetas_establecimiento = idtarjetas_establecimiento;
	}
	public long getIdtarjetas_establecimiento() {
		return idtarjetas_establecimiento;
	}
	
	public Long getId_previvale_card_stock() {
		return id_previvale_card_stock;
	}

	public void setId_previvale_card_stock(Long id_previvale_card_stock) {
		this.id_previvale_card_stock = id_previvale_card_stock;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public String getId_stp_cuentas_clabe() {
		return id_stp_cuentas_clabe;
	}

	public void setId_stp_cuentas_clabe(String id_stp_cuentas_clabe) {
		this.id_stp_cuentas_clabe = id_stp_cuentas_clabe;
	}
	
	

}
