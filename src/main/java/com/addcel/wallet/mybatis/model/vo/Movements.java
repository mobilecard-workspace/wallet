package com.addcel.wallet.mybatis.model.vo;

import java.util.List;

import com.addcel.wallet.client.model.vo.McResponse;

public class Movements extends McResponse {

	private List<MovementDetail> month;
	private List<MovementDetail> previous_month;
	private List<MovementDetail> today;
	private List<MovementDetail> week;
	
	public Movements() {
		// TODO Auto-generated constructor stub
	}

	public List<MovementDetail> getMonth() {
		return month;
	}

	public void setMonth(List<MovementDetail> month) {
		this.month = month;
	}

	public List<MovementDetail> getPrevious_month() {
		return previous_month;
	}

	public void setPrevious_month(List<MovementDetail> previous_month) {
		this.previous_month = previous_month;
	}

	public List<MovementDetail> getToday() {
		return today;
	}

	public void setToday(List<MovementDetail> today) {
		this.today = today;
	}

	public List<MovementDetail> getWeek() {
		return week;
	}

	public void setWeek(List<MovementDetail> week) {
		this.week = week;
	}
	

}
