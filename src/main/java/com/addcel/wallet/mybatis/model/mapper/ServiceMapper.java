package com.addcel.wallet.mybatis.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.wallet.client.model.vo.MobileCardCard;
import com.addcel.wallet.mybatis.model.vo.Card;
import com.addcel.wallet.mybatis.model.vo.Estados;
import com.addcel.wallet.mybatis.model.vo.Login;
import com.addcel.wallet.mybatis.model.vo.ProjectMC;
import com.addcel.wallet.mybatis.model.vo.User;

//import com.addcel.ingo.mcingo.mybatis.model.vo.ClienteVO;
//import com.addcel.ingo.mcingo.mybatis.model.vo.DataUser;
//import com.addcel.ingo.mcingo.mybatis.model.vo.User;



public interface ServiceMapper {

	public String ProcessCard(HashMap parameters);
	public Login getLogin(long id_usuario);
	public List<Estados> getEstates(int idpais);
	public User getUser(long id_usuario);
	public void updateCustomerId(User user);
	public Card getMobilecardCard(long id_usuario);
	public Estados getState(@Param(value = "abb") String abb, @Param(value = "id") int id);
	public String getMovements(@Param(value = "idUsuario") long idUsuario);
	public String getParameter(@Param(value = "parameter") String parameter);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	//public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
//	public String getCustumerID(@Param(value = "user") String user, @Param(value = "pass") String pass);
	
	//public String  getUser(HashMap datauser);
	//public void updateCustomerId(User user);
	
}
