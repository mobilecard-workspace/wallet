package com.addcel.wallet.mybatis.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.wallet.mybatis.model.vo.AppCipher;
import com.addcel.wallet.mybatis.model.vo.Card;
import com.addcel.wallet.mybatis.model.vo.CardCommerce;
import com.addcel.wallet.mybatis.model.vo.CardImage;
import com.addcel.wallet.mybatis.model.vo.Commerce;
import com.addcel.wallet.mybatis.model.vo.Estados;
import com.addcel.wallet.mybatis.model.vo.Login;
import com.addcel.wallet.mybatis.model.vo.PrevivaleStock;
import com.addcel.wallet.mybatis.model.vo.ProjectMC;
import com.addcel.wallet.mybatis.model.vo.User;

public interface WalletMapper {

	public String ProcessCard(HashMap parameters);

	public Login getLogin(@Param(value = "id_usuario") long id_usuario, @Param(value = "idApp") int idApp);

	public User getUser(@Param(value = "id_usuario") long id_usuario, @Param(value = "idApp") int idApp);

	public Estados getState(@Param(value = "abb") String abb, @Param(value = "id") int id);

	public void updateCustomerId(User user);

	public Card getMobilecardCard(@Param(value = "id_usuario") long id_usuario, @Param(value = "idApp") int idApp);

	public String getParameter(@Param(value = "parameter") String parameter);

	public ProjectMC getProjectMC(@Param(value = "cont") String cont, @Param(value = "recu") String recu);

	public List<Estados> getEstates(int idpais);

	public String getMovements(@Param(value = "idUsuario") long idUsuario, @Param(value = "idApp") int idApp);

	public void updateUser(User user);

	public void updateUserDataPrevivale(User user);
	
	public Integer getValidPrevivaleCard(@Param(value = "id_usuario") long id_usuario);
	
	public PrevivaleStock GetPrevivaleCardStock(@Param(value = "id") long id);
	
	public PrevivaleStock GetPrevivaleCardStockByCard(@Param(value = "pan") String pan);
	
	public void UpdateCommerce(Commerce commerce);
	
	public CardCommerce getCardCommerce(@Param(value = "id") long id,@Param(value = "id_aplicacion") long id_aplicacion);
	
	public AppCipher getAppCipher(@Param(value = "id") Integer id);
	
	public String getCuentaClabeMobilecard(@Param(value = "id") long id);
	
	public String getCuentaClabeMobilecardByIdTarjeta(@Param(value = "id") long id);
	
	public CardImage getCardImage(@Param(value = "bin") String bin);
	
	public String isAppActive(@Param(value = "idApp") Integer idApp);
	 
	public String getTransactionPerMonth(@Param(value = "idUsuario") long idUsuario, @Param(value = "idApp") int idApp, @Param(value = "idPais")  int idPais ,@Param(value = "idioma") String idioma);
	
	public String getNameFranquicia(@Param(value = "pan") String pan);
	
	public Integer hasMobilecard(@Param(value = "idUsuario") long idUsuario,@Param(value = "idApp") int idApp);
}
