package com.addcel.wallet.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.utils.AddcelCrypto;
import com.addcel.wallet.client.model.vo.EstadosResponse;
import com.addcel.wallet.client.model.vo.MobileCardCard;
import com.addcel.wallet.client.model.vo.MobilecardCardResponse;
import com.addcel.wallet.client.model.vo.TarjetaRequest;
import com.addcel.wallet.client.model.vo.TarjetaRequestMC;
import com.addcel.wallet.client.model.vo.TarjetaResponse;
import com.addcel.wallet.mybatis.model.vo.Estados;
import com.addcel.wallet.mybatis.model.vo.Movements;
import com.addcel.wallet.services.ServicesCard;
import com.addcel.wallet.services.UtilsService;



@Controller
public class RestServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServices.class);
	
	@Autowired
	private ServicesCard servicesCard;
	
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public ModelAndView getToken() {
	//	System.out.println(UtilsService.getSMS("oj0aajEBys+nG8L2MKJvBA=="));
		//System.out.println(UtilsService.setSMS("05/21"));
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}

	@RequestMapping(value = "mobilecard/delete", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> delete(@RequestParam("idTarjeta") int idTarjeta , @RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.delete(idTarjeta, idUsuario,idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/update", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> update(@RequestBody  TarjetaRequest cardRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.update(cardRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/getTarjetas", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> get(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		
		//servicesCard.CreateCardFV("MAriana", "Lopez", "Arapahoe Av. 41", "CO", "Boulder", "019283", "hola@gamil.com", "3344332211", "04071988", "111111122", 123456531);
		
		return  new ResponseEntity<TarjetaResponse>(servicesCard.getCards(idUsuario, idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/add", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> Add(@RequestBody  TarjetaRequest cardRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.addCard(cardRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/addmobilecard", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> addCardMobilecard(@RequestBody TarjetaRequestMC carRequest) {
		return  new ResponseEntity<TarjetaResponse>(servicesCard.AddMobileCardCard2(carRequest),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/{idpais}/estados", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<EstadosResponse> getEstados(@PathVariable int idpais) {
		return  new ResponseEntity<EstadosResponse>(servicesCard.getEstates(idpais),HttpStatus.OK);
	}
	
	@RequestMapping(value = "mobilecard/movements", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Movements> getMovements(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma) {
		return  new ResponseEntity<Movements>(servicesCard.GetMovements(idUsuario, idioma),HttpStatus.OK);
	}
	
	@RequestMapping(value="mobilecard/getmobilecard/", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<MobilecardCardResponse> getMobilecard(@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma
			, @RequestParam("pais") int pais){
		return new ResponseEntity<MobilecardCardResponse>(servicesCard.getMobilecard(idUsuario, idioma, pais),HttpStatus.OK);
	}
	
	
}
