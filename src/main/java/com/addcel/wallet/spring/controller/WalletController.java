package com.addcel.wallet.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.addcel.wallet.client.model.vo.DefaultCardReq;
import com.addcel.wallet.client.model.vo.EstadosResponse;
import com.addcel.wallet.client.model.vo.McResponse;
import com.addcel.wallet.client.model.vo.MobilecardCardResponse;
import com.addcel.wallet.client.model.vo.TarjetaRequest;
import com.addcel.wallet.client.model.vo.TarjetaRequestMC;
import com.addcel.wallet.client.model.vo.TarjetaResponse;
import com.addcel.wallet.client.model.vo.previvaleUser;
import com.addcel.wallet.mybatis.model.vo.Movements;
import com.addcel.wallet.mybatis.model.vo.User;
import com.addcel.wallet.services.WalletServices;

@Controller
public class WalletController {

	private static final String PATH_CARD_REGISTER_PREVIVALE = "{idApp}/{idPais}/{idioma}/{tipo}/{id}/registrartarjetaprevivale";
	
	private static final String PATH_ACTIVATE_DEFAULT_CARD = "{idApp}/{idPais}/{idioma}/defaultcard";
	
	private static final String PATH_USER_TRANSACTION_PER_MONTH = "{idApp}/{idPais}/{idioma}/transactions";
	
	
	@Autowired
	private WalletServices WServices;
	
	@RequestMapping(value = "{idApp}/delete", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> delete(@PathVariable int idApp ,@RequestParam("idTarjeta") int idTarjeta , @RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<TarjetaResponse>(WServices.delete(idTarjeta, idUsuario,idioma, idApp),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = "{idApp}/update", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> update(@RequestBody  TarjetaRequest cardRequest, @PathVariable int idApp ) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<TarjetaResponse>(WServices.update(cardRequest, idApp),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	// {idApp}/getTarjetas
	/*@RequestMapping(value = "{idApp}/getCards/with_movements", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getCards(@PathVariable int idApp,@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		
		return  new ResponseEntity<TarjetaResponse>(WServices.getCards(idUsuario, idioma, idApp),HttpStatus.OK);
	}*/
	
	@RequestMapping(value = "{idApp}/getCards/{filter}", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getCards(@PathVariable String  filter,@PathVariable int idApp,@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		
		if(WServices.isActive(idApp)){
			if(filter.equalsIgnoreCase("with_movements"))
				return  new ResponseEntity<TarjetaResponse>(WServices.getCards(idUsuario, idioma, idApp),HttpStatus.OK);
			else
				return  new ResponseEntity<TarjetaResponse>(WServices.WithoutMovement(idUsuario, idioma, idApp, 7,Integer.parseInt(filter)),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}	
	}
	
	//get tarjetas sin movimientos
	@RequestMapping(value = "{idApp}/getCards", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getCardWithoutMovement(@PathVariable int idApp,@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma ) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<TarjetaResponse>(WServices.WithoutMovement(idUsuario, idioma, idApp),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}	
	}
	
	@RequestMapping(value = "{idApp}/add", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> Add(@PathVariable int idApp,@RequestBody  TarjetaRequest cardRequest) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<TarjetaResponse>(WServices.addCard(cardRequest, idApp),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}	
	}
	
	@RequestMapping(value = "{idApp}/addmobilecard", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> addCardMobilecard(@PathVariable int idApp,@RequestBody TarjetaRequestMC carRequest) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<TarjetaResponse>(WServices.AddMobileCardCard2(carRequest, idApp),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = "{idApp}/{idpais}/estados", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<EstadosResponse> getEstados(@PathVariable int idApp,@PathVariable int idpais) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<EstadosResponse>(WServices.getEstates(idpais,idApp),HttpStatus.OK);
		}else{
			EstadosResponse error = new EstadosResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<EstadosResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	//OBTENER TODOS LOS MOVIMIENTOS DEL USUARIO
	@RequestMapping(value = "{idApp}/movements", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Movements> getMovements(@PathVariable int idApp,@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma) {
		if(WServices.isActive(idApp)){
			return  new ResponseEntity<Movements>(WServices.GetMovements(idUsuario, idioma, idApp),HttpStatus.OK);
		}
		else{
			Movements error = new Movements();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<Movements>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	//OBTENER TARJETA MOBILECARD
	@RequestMapping(value="{idApp}/getCustomCard", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<MobilecardCardResponse> getMobilecard(@PathVariable int idApp,@RequestParam("idUsuario") long idUsuario, @RequestParam("idioma") String idioma
			, @RequestParam("pais") int pais){
		if(WServices.isActive(idApp)){
			return new ResponseEntity<MobilecardCardResponse>(WServices.getMobilecard(idUsuario, idioma, pais,idApp),HttpStatus.OK);
		}else{
			MobilecardCardResponse error = new MobilecardCardResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<MobilecardCardResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value="{idApp}/{idPais}/{idioma}/getPrevivaleCard", method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<MobilecardCardResponse> getPrevivaleCard(@PathVariable int idApp, @PathVariable int idPais, 
			@PathVariable String idioma, @RequestBody User user){
		if(WServices.isActive(idApp)){
			return new ResponseEntity<MobilecardCardResponse>(WServices.getPrevivaleCard(idioma, idPais, idApp, user),HttpStatus.OK);
		}else{
			MobilecardCardResponse error = new MobilecardCardResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<MobilecardCardResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_CARD_REGISTER_PREVIVALE, method = RequestMethod.POST)
	public ResponseEntity<MobilecardCardResponse> AgregarTarjeta(@RequestBody previvaleUser user, @PathVariable String tipo, @PathVariable long id, @PathVariable String idioma, @PathVariable Integer idApp, @PathVariable Integer idPais){
		if(WServices.isActive(idApp)){
			return new ResponseEntity<MobilecardCardResponse>(WServices.registrarTarjetaPrevivale(user, idPais, idApp, idioma, tipo, id),HttpStatus.OK);
		}else{
			MobilecardCardResponse error = new MobilecardCardResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<MobilecardCardResponse>(error,HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = PATH_ACTIVATE_DEFAULT_CARD, method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<TarjetaResponse> getActivateDeafultCard(@PathVariable int idApp, @PathVariable int idPais, 
																	@PathVariable String idioma, @RequestBody DefaultCardReq req) {
		if(WServices.isActive(idApp)){	
			return  new ResponseEntity<TarjetaResponse>(WServices.activateDefaultCard(idApp, idPais, idioma, req),HttpStatus.OK);
		}else{
			TarjetaResponse error = new TarjetaResponse();
			error.setIdError(401);
			error.setMensajeError("No estas autorizado para realizar la operacion");
			return  new ResponseEntity<TarjetaResponse>(error,HttpStatus.UNAUTHORIZED);
		}
			
			
	}
	
	@RequestMapping(value = PATH_USER_TRANSACTION_PER_MONTH, method=RequestMethod.POST,produces = "application/json")
	public ResponseEntity<Object> getTransactionPerMonth(@PathVariable int idApp, @PathVariable int idPais, 
			@PathVariable String idioma, @RequestParam("idUsuario") long idUsuario){
		if(WServices.isActive(idApp)){	
			
			return  new ResponseEntity<Object>(WServices.getTransactionPerMonth(idUsuario, idApp, idPais, idioma),HttpStatus.OK);
			
		}else{
			
			return  new ResponseEntity<Object>(new McResponse(401,"No estas autorizado para realizar la operacion"),HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	
}
