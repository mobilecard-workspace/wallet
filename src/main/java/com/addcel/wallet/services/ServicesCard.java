package com.addcel.wallet.services;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.wallet.client.model.vo.EstadosResponse;
import com.addcel.wallet.client.model.vo.McResponse;
import com.addcel.wallet.client.model.vo.MobileCardCard;
import com.addcel.wallet.client.model.vo.MobilecardCardResponse;
import com.addcel.wallet.client.model.vo.TarjetaRequest;
import com.addcel.wallet.client.model.vo.TarjetaRequestMC;
import com.addcel.wallet.client.model.vo.TarjetaResponse;
import com.addcel.wallet.client.viamericas.ProfileCardRequest;
import com.addcel.wallet.client.viamericas.ViamericasClient;
import com.addcel.wallet.mybatis.model.mapper.ServiceMapper;
import com.addcel.wallet.mybatis.model.vo.Card;
import com.addcel.wallet.mybatis.model.vo.Estados;
import com.addcel.wallet.mybatis.model.vo.Login;
import com.addcel.wallet.mybatis.model.vo.Movements;
import com.addcel.wallet.mybatis.model.vo.ProjectMC;
import com.addcel.wallet.mybatis.model.vo.User;
import com.addcel.wallet.utils.Constants;
import com.addcel.wallet.utils.UtilsMC;
import com.google.gson.Gson;
import com.addcel.firstView.service.ServicesProxy;
import com.addcel.firstView.service.model.CreateCard;
import com.addcel.firstView.service.model.CreateCardResponse;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequest;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerRequest;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequest;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.ingo.bridge.client.model.vo.IngoNotifications;
import com.addcel.utils.AddcelCrypto;


@Service
public class ServicesCard {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServicesCard.class);
	
	@Autowired
	private ServiceMapper mapper;
	
	@Autowired
	private MCIngorequest mcIngorequest;
	
	@Autowired
	ViamericasClient viamericasClient;
	
	private Gson gson = new Gson();
	
	
	
	
	
	public TarjetaResponse addCard(TarjetaRequest cardRequest){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE INSERCION DE TARJETA {} ", cardRequest);
			Login login = mapper.getLogin(cardRequest.getIdUsuario()); 
			String pan =  AddcelCrypto.decryptHard(cardRequest.getPan());
			String vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
			
			String cod = cardRequest.getCodigo() == null ? "" : AddcelCrypto.decryptHard(cardRequest.getCodigo());
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", pan);
			parameters.put("nnumtarjetacrip", UtilsService.setSMS(pan));
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 1);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex() );
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("mobilecard", cardRequest.isMobilecard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
			
			
			LOGGER.debug("parameters add card: " + parameters.get("nproceso") );
			
			String responseAdd = mapper.ProcessCard(parameters);
			McResponse add_response = gson.fromJson(responseAdd, McResponse.class);
			
			LOGGER.debug("Respuesta add: " + responseAdd);	
			parameters.replace("nproceso", 3);
			
			response = getCards(parameters,cardRequest.getIdUsuario());
			response.setIdError(add_response.getIdError());
			response.setMensajeError(add_response.getMensajeError());
			
			
			//VIAMERICAS SOLO SI ES USA
			if(login.getIdpais() == 3)
				CreateProfileViamericas(cardRequest.getDomAmex(), cardRequest.getNombre().split(" ")[0], cardRequest.getNombre().split(" ")[1], pan, cod, vig.replace("/", ""), login.getId_sender(), cardRequest.getIdUsuario(), login.getUser().split("@")[0], "K");
			
			
			LOGGER.debug("FINALIZANDO PROCESO DE INSERCION DE TRAJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir tarjeta : " + ex.getMessage());
			LOGGER.error("Error:", ex);
			return LogService.getAddMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}
	
	private void CreateProfileViamericas(String address, String cardFirstName, String cardLastName, String cardNumber, String cvv, String expDate, String idSender, long idUsuario, String nickName, String paymentType){
		try{
			if(idSender != null && !idSender.isEmpty())
			{
				ProfileCardRequest request = new ProfileCardRequest();
				request.setAddress(address);
				request.setCardFirstName(cardFirstName);
				request.setCardLastName(cardLastName);
				request.setCardNumber(cardNumber);
				request.setCvv(cvv);
				request.setExpDate(expDate);
				request.setIdSender(idSender);
				request.setIdUsuario(idUsuario);
				request.setNickName(nickName);
				request.setPaymentType(paymentType);
			
				viamericasClient.createSenderProfileCard(request);
			}
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL CREAR PROFILE VIAMERICAS]", ex);
		}
	}
	 
	public TarjetaResponse update(TarjetaRequest cardRequest){
		TarjetaResponse response= new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE ACTUALIZACION");
			Login login = mapper.getLogin(cardRequest.getIdUsuario());
			HashMap parameters = new HashMap<>();
			
			String vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
			String cod =  AddcelCrypto.decryptHard(cardRequest.getCodigo());
			
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 5);
			parameters.put("nidtarjetausuario", cardRequest.getIdTarjeta());
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex());
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("mobilecard", cardRequest.isMobilecard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
			
			//LOGGER.debug("IsMobileCard: " + cardRequest.isMobilecard());
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("Campos actualizados... " + responseJson);
			
			McResponse mcResponseUpdate = gson.fromJson(responseJson, McResponse.class);
			
			
			if(mcResponseUpdate.getIdError() == 0 && cardRequest.isDeterminada())
			{
				parameters.replace("nproceso", 2);
				McResponse mcResponse = active(parameters);
				LOGGER.debug("activando");
				if(mcResponse.getIdError()!= 0){
					response.setIdError(mcResponse.getIdError());
					response.setMensajeError(mcResponse.getMensajeError());
				}
				
			}
			
			parameters.put("nidtarjetausuario", 1); ///para saber si es de mexico la peticion
			parameters.replace("nproceso", 3);
			LOGGER.debug("cards obtenidas...");
		//	responseJson = getCards(parameters);
			response = getCards(parameters,cardRequest.getIdUsuario());//gson.fromJson(responseJson,TarjetaResponse.class);
			response.setIdError(mcResponseUpdate.getIdError());
			response.setMensajeError(mcResponseUpdate.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE ACTUALIZACION");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir actualizar tarjeta : " + ex.getMessage());
			response.setIdError(101);
			response.setMensajeError("error al actualizar tarjeta");
			return LogService.CardUpdateMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}
	
	
	
	
	
	public TarjetaResponse getCards(long idUsuario, String idioma){
		try{
			LOGGER.debug("INICIANDO PROCESO GETCARDS");
			Login login = mapper.getLogin(idUsuario); 
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 3);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", "");
			LOGGER.debug("fINALIZANDO PROCESO GETCARDS");
			return getCards(parameters,idUsuario);
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : ", ex);
			return LogService.getCardsMessageerror(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public MobilecardCardResponse getMobilecard(long idUsuario, String idioma, int pais){
		MobilecardCardResponse card = new MobilecardCardResponse();
		
		try{
			LOGGER.debug("[INICIANDO PROCESO DE OBTENCION DE TARJETA MOBILECARD]");
			TarjetaResponse cardResponse = getCards(idUsuario, idioma);
			int size = cardResponse.getTarjetas().size();
			for(int i=0; i<size; i++){
				if(cardResponse.getTarjetas().get(i).isMobilecard()){
					card.setCard(cardResponse.getTarjetas().get(i));
					card.setIdError(cardResponse.getIdError());
					card.setMensajeError(cardResponse.getMensajeError());
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL OBTENER TARJETA MOBILECARD]",ex);
			card.setIdError(99);
			card.setMensajeError("General Error, try again");
		}
		
		return card;
	}
	
	
	public TarjetaResponse delete(int idTarjeta, long idUsuario, String idioma){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE BORRADO DE TARJETA");
			Login login = mapper.getLogin(idUsuario); 
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", login.getUser());
			parameters.put("nclave",login.getPass() );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 4);
			parameters.put("nidtarjetausuario", idTarjeta);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", "");
			TarjetaResponse responseDelete = delete(parameters);
			
			parameters.replace("nproceso", 3);
			parameters.replace("nidtarjetausuario", 1);
			
			response = getCards(parameters, idUsuario);
			
			response.setIdError(responseDelete.getIdError());
			response.setMensajeError(responseDelete.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE BORRADO DE TARJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : " + ex.getMessage());
			
			return LogService.CardDeleteMessageError(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public EstadosResponse getEstates(int idpais)
	{
		List<Estados> estados = mapper.getEstates(idpais);
		
		EstadosResponse response = new EstadosResponse();
		response.setEstados(estados);
		
		
		if(estados != null && estados.size() > 0)
		{
			response.setIdError(0);
			response.setMensajeError("");
		}
		else
		{
			response.setIdError(-1);
			if(idpais == 1)
				response.setMensajeError("País sin estados en catálogo");
			else
				response.setMensajeError("Country without states in catalog");
		}
		
		
		LOGGER.debug("Estados Count: " + estados.size());
		
		return response;
	}
	
	public Movements GetMovements(long idUsuario,String idioma){
		
		Movements movements = new Movements();
		try{
			
			String json =  mapper.getMovements(idUsuario);
			movements = gson.fromJson(json, Movements.class);
			movements.setIdError(0);
			movements.setMensajeError("");
			
		}catch(Exception ex)
		{
			LOGGER.error("Error: ", ex);
			movements.setIdError(99);
			if(!idioma.equals("en"))
				movements.setMensajeError("Error en base de datos");
			else
				movements.setMensajeError("DataBase error");
		}
		
		
		return movements;
	}
	
	public TarjetaResponse AddMobileCardCard2(TarjetaRequestMC carRequest){
		TarjetaResponse response =  new TarjetaResponse();
		CustomerResponse responseIngo = new CustomerResponse();
		User user = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); 
		String expira = "";
		String anio = "";
		String mes = "";
		Card mobilecardCard = null;
		CreateCardResponse responseFV = new CreateCardResponse();
		GetRegisteredCardsRequestMC resgisterCards = new GetRegisteredCardsRequestMC();
		int cards = 0;
		
		try{
			user= mapper.getUser(carRequest.getIdUsuario());
			user.setUsr_direccion(carRequest.getDireccion());
			user.setUsr_ciudad(carRequest.getCiudad());
			user.setUsr_cp(carRequest.getCp());
			user.setUsr_fecha_nac(new java.sql.Date(sdf.parse(carRequest.getFechaNac()).getTime()));
			user.setUsr_nss(carRequest.getSsn());
			user.setUsr_id_estado(mapper.getState(carRequest.getEstado(), -1).getId());
			
			//enrrolar usuario en ingo
			if(user.getId_ingo() == null || user.getId_ingo().isEmpty() ){	
				responseIngo = EnrollCustomer(carRequest.getDireccion(), carRequest.getCiudad(),carRequest.getFechaNac().substring(4,8)+"-"+carRequest.getFechaNac().substring(2,4)+"-"+carRequest.getFechaNac().substring(0,2),user.geteMail(), user.getUsr_nombre(), user.getUsr_apellido(), user.getUsr_telefono(), carRequest.getSsn(), carRequest.getEstado(),carRequest.getCp(),user.getImei());
				user.setId_ingo(responseIngo.getCustomerId());
				mapper.updateCustomerId(user);
			}
			else
			{
				LOGGER.debug("Usuario ya se encuentra en INGO... [RecuPerando Datos]");
				responseIngo.setCustomerId(user.getId_ingo());
				mobilecardCard = mapper.getMobilecardCard(user.getId_usuario());
			}
			
			 
			//Checar si tiene tajetas en ingo
			resgisterCards.setCustomerId(responseIngo.getCustomerId());
			resgisterCards.setDeviceId(user.getImei());
			//CardsResponse cards_response =  mcIngorequest.GetRegisteredCards(resgisterCards, resgisterCards.getDeviceId());
			cards = 0;//cards_response.getCards().size();
			//validando que no tenga registrada tarjeta MC en ingo
			if(cards == 0 )
			{
					//crear tarjeta FV
					if(mobilecardCard == null || mobilecardCard.getPan() == null )
						responseFV = CreateCardFV(user.getUsr_nombre(),user.getUsr_apellido(),carRequest.getDireccion(),carRequest.getEstado(),carRequest.getCiudad(),carRequest.getCp(), user.geteMail(),user.getUsr_telefono(),carRequest.getFechaNac().substring(2,4) + carRequest.getFechaNac().substring(0,2) + carRequest.getFechaNac().substring(4,carRequest.getFechaNac().length()),carRequest.getSsn(),carRequest.getIdUsuario(),carRequest.getGovtId(), carRequest.getGovtIdExpirationDate(),carRequest.getGovtIdIssueDate(),carRequest.getGovtIdIssueState());
					else
					{
						//Recuperar datos de tarjeta del wallet
						responseFV.setCardNumber(UtilsService.getSMS(mobilecardCard.getPan()));
						responseFV.setCvv(UtilsService.getSMS(mobilecardCard.getCodigo()));
						responseFV.setErrorCode(0);
						responseFV.setErrorDescription("");
						responseFV.setExpirationDate(UtilsService.getSMS(mobilecardCard.getVigencia()));
						responseFV.setIdUsuario(user.getId_usuario());
						
					}
					
					if(responseFV.getErrorCode() == 0 && responseFV.getCardNumber()!=null && responseFV.getCvv()!= null && responseFV.getExpirationDate() != null){
						
						 mes = responseFV.getExpirationDate().substring(0,2);
						 anio = responseFV.getExpirationDate().substring(responseFV.getExpirationDate().length()-2);
						 expira = mes+anio; //MMYY
						
						if(responseIngo.getCustomerId() != null && !responseIngo.getCustomerId().isEmpty())
						{
							//agregar targeta a Ingo
							resgisterCards.setCustomerId(responseIngo.getCustomerId());
							resgisterCards.setDeviceId(user.getImei());
							
							//
					        CardsResponse responseCardIngo = addCardIngo(user.getUsr_direccion(), user.getUsr_nombre(), responseFV.getCardNumber(), carRequest.getCiudad(), responseIngo.getCustomerId(), user.getImei(),expira , (user.getUsr_nombre().trim() + " " + user.getUsr_apellido()).trim() , carRequest.getEstado(), user.getUsr_cp());
					        
					        if(responseCardIngo.getErrorCode() == 0){
								LOGGER.debug("Se Agrego correctamente la tarjeta al usuario {}",responseIngo.getCustomerId() );
							}
							else
							{
								LOGGER.debug("Error al agregar tarjeta al usuario {} message {} ", responseIngo.getCustomerId(), responseCardIngo.getErrorMessage());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
					        
						}else
						{
							LOGGER.debug("Error al enrrolar usuario en ingo {} {}", carRequest.getIdUsuario(), responseIngo.getErrorMessage());
							response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
						}
						
						
						//agregar Tarjeta al wallet
						if(mobilecardCard == null || mobilecardCard.getPan() == null)
						{
							TarjetaRequest cardRequest = new TarjetaRequest();
							//cardRequest.setCodigo(responseFV.getCvv());
							cardRequest.setCpAmex("");
							cardRequest.setDeterminada(false);
							cardRequest.setDomAmex("");
							cardRequest.setIdioma(carRequest.getIdioma());
							cardRequest.setIdTarjeta(0);
							cardRequest.setIdUsuario(user.getId_usuario());
							cardRequest.setMobilecard(true);
							cardRequest.setNombre(user.getUsr_nombre().toString() + " " + user.getUsr_apellido().trim());
							cardRequest.setPan(AddcelCrypto.encryptHard(responseFV.getCardNumber()));
							cardRequest.setTipo(0);
							cardRequest.setVigencia(AddcelCrypto.encryptHard(mes+"/"+anio));
							cardRequest.setCodigo(AddcelCrypto.encryptHard(responseFV.getCvv()));
							cardRequest.setTipoTarjeta(Constants.CREDIT_CARD_TYPE);
							
							response = addCard(cardRequest);
							
							if(response.getIdError() == 0){
								LOGGER.debug("Se agrego la Tarjeta correctamente al Wallet");
								response.setMensajeError(response.getMensajeError());
								ProjectMC project =  mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
								//enviando email de confirmacion
								if(carRequest.getIdioma().equalsIgnoreCase("es"))
									UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_MOBILECARD_CARD_ES"), mapper.getParameter("@ASUNTO_MOBILECARD_CARD_ES"), "", "", UtilsMC.maskCard(responseFV.getCardNumber(), "X"), cardRequest.getNombre(), new String[]{Constants.Path_images+"user_mobilecard_card.PNG"},project.getUrl());
								else
									UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_MOBILECARD_CARD_EN"), mapper.getParameter("@ASUNTO_MOBILECARD_CARD_EN"), "", "", UtilsMC.maskCard(responseFV.getCardNumber(), "X"), cardRequest.getNombre(), new String[]{Constants.Path_images+"user_mobilecard_card.PNG"},project.getUrl());
								
							}
							else{
								LOGGER.debug("Error al agregar tarejta al wallet {} {}", response.getMensajeError(), cardRequest.getIdUsuario());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
						}else
							LOGGER.debug("LA TARJETA YA SE ENCONTRABA EN EL WALLET...");
						
						
					}else{
						LOGGER.debug("Error al crear Targeta FirstView {} {} " + carRequest.getIdUsuario(), responseFV.getErrorDescription());
						response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2); //LogService.ErrorFV(carRequest.getIdioma().equals("es") ? 1 : 2);
					}
			}
			else
			{
				LOGGER.debug("Se detecto un error con una tarjeta registrada previamente, contacte al administrador");
				response.setIdError(110);
				response.setMensajeError("An error was detected with a previously registered card, contact technical support"); //Se detecto un error con una tarjeta registrada previamente, contacte a soporte tecnico
				response.setTarjetas(new ArrayList<MobileCardCard>());
			}
			
		}catch(Exception ex){
			LOGGER.error("Error al crear tarjetaMobileCard", ex);
			
			return LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
		}
		return response;
		
	}
	
	/*public TarjetaResponse AddMobileCardCard(TarjetaRequestMC carRequest){
		TarjetaResponse response =  new TarjetaResponse();
		User user = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); 
		String expira = "";
		String anio = "";
		String mes = "";
		try{
			
			LOGGER.debug("Añadiendo tarjeta MobileCard {}", carRequest);
			
			user= mapper.getUser(carRequest.getIdUsuario());
			user.setUsr_direccion(carRequest.getDireccion());
			user.setUsr_ciudad(carRequest.getCiudad());
			user.setUsr_cp(carRequest.getCp());
			user.setUsr_fecha_nac(new java.sql.Date(sdf.parse(carRequest.getFechaNac()).getTime()));
			user.setUsr_nss(carRequest.getSsn());
			
			//Crear Tarjeta FV
			CreateCardResponse responseFV = CreateCardFV(user.getUsr_nombre(),user.getUsr_apellido(),carRequest.getDireccion(),carRequest.getEstado(),carRequest.getCiudad(),carRequest.getCp(), user.geteMail(),user.getUsr_telefono(),carRequest.getFechaNac(),carRequest.getSsn(),carRequest.getIdUsuario());
			
			if(responseFV.getErrorCode() == 0 && responseFV.getCardNumber()!=null && responseFV.getCvv()!= null && responseFV.getExpirationDate() != null){
				
				 mes = responseFV.getExpirationDate().substring(0,2);
				 anio = responseFV.getExpirationDate().substring(responseFV.getExpirationDate().length()-2);
				 expira = mes+anio; //MMYY

				
				//Enrrolar con Ingo	
				CustomerResponse responseIngo = EnrollCustomer(carRequest.getDireccion(), carRequest.getCiudad(),carRequest.getFechaNac().substring(4,8)+"-"+carRequest.getFechaNac().substring(2,4)+"-"+carRequest.getFechaNac().substring(0,2),user.geteMail(), user.getUsr_nombre(), user.getUsr_apellido(), user.getUsr_telefono(), carRequest.getSsn(), carRequest.getEstado(),carRequest.getCp(),user.getImei());
				
				if(responseIngo.getErrorCode() == 0){
					
					user.setId_ingo(responseIngo.getCustomerId());
			        mapper.updateCustomerId(user);
			        
					//agregar targeta Ingo
			        CardsResponse responseCardIngo = addCardIngo(user.getUsr_direccion(), user.getUsr_nombre(), responseFV.getCardNumber(), carRequest.getCiudad(), responseIngo.getCustomerId(), user.getImei(),expira , (user.getUsr_nombre() + " " + user.getUsr_apellido()).trim() , carRequest.getEstado(), user.getUsr_cp());
					
					if(responseCardIngo.getErrorCode() == 0){
						LOGGER.debug("Se Agrego correctamente la tarjeta al usuario {}",responseIngo.getCustomerId() );
					}
					else
					{
						LOGGER.debug("Error al agregar tarjeta al usuario {} message {} ", responseIngo.getCustomerId(), responseCardIngo.getErrorMessage());
						response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
					}
				}
				else
				{
					LOGGER.debug("Error al enrrolar usuario en ingo {} {}", carRequest.getIdUsuario(), responseIngo.getErrorMessage());
					response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
				}
				
				//agregar Tarjeta al wallet
				TarjetaRequest cardRequest = new TarjetaRequest();
				cardRequest.setCodigo(responseFV.getCvv());
				cardRequest.setCpAmex("");
				cardRequest.setDeterminada(false);
				cardRequest.setDomAmex("");
				cardRequest.setIdioma(carRequest.getIdioma());
				cardRequest.setIdTarjeta(0);
				cardRequest.setIdUsuario(user.getId_usuario());
				cardRequest.setMobilecard(true);
				cardRequest.setNombre(user.getUsr_nombre().toString() + " " + user.getUsr_apellido().trim());
				cardRequest.setPan(AddcelCrypto.encryptHard(responseFV.getCardNumber()));
				cardRequest.setTipo(0);
				cardRequest.setVigencia(AddcelCrypto.encryptHard(mes+"/"+anio));
				cardRequest.setCodigo(AddcelCrypto.encryptHard(responseFV.getCvv()));
				
				response = addCard(cardRequest);
				
				if(response.getIdError() == 0){
					LOGGER.debug("Se agrego la Tarjeta correctamente al Wallet");
				}
				else{
					LOGGER.debug("Error al agregar tarejta al wallet {} {}", response.getMensajeError(), cardRequest.getIdUsuario());
					response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
				}
				
			}
			else{
				LOGGER.debug("Error al crear Targeta FirstView {} {} " + carRequest.getIdUsuario(), responseFV.getErrorDescription());
				response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2); //LogService.ErrorFV(carRequest.getIdioma().equals("es") ? 1 : 2);
			}
			
			
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al crear tarjetaMobileCard", ex);
			return LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
		}
		
	}*/
	
	private CustomerResponse EnrollCustomer(String address, String city, String dateOfBirth, String email, String firstName, String lastName, String mobileNumber, String ssn, String state, String zip, String deviceId){
		
		EnrollCustomerMC request = new EnrollCustomerMC();
		//EnrollCustomerRequest request = new EnrollCustomerRequest();
		CustomerResponse response = null;
			
		request.setAddressLine1(address);
		request.setAddressLine2("");
		request.setAllowTexts(true);
		request.setCity(city);
		request.setCountryOfOrigin(""); //optional 
		request.setDateOfBirth(dateOfBirth); //yyyy-MM-dd (e.g., 1978-09-18) 1994-07-02 00:00:00 
		request.setEmail(email);
		request.setFirstName(firstName);
		request.setGender(""); //Optional Single character string indicating M/F.
		request.setHomeNumber("");
		request.setLastName(lastName);
		request.setMiddleInitial(""); //Optional Single character middle initial. Max length is 1 character.
		request.setMobileNumber(mobileNumber);
		request.setSsn(ssn);
		request.setState(state);
		request.setSuffix(""); // optional Typically would contain one of Jr, Sr, etc.
		request.setTitle(""); // optional Typically would contain one of Mrs, Mr, etc.
		request.setZip(zip);
		request.setDeviceId(deviceId);
		
		
		response =  mcIngorequest.EnrollCustomer(request, deviceId);
		
		return response;
	}
	
	private CardsResponse addCardIngo(String address, String cardNickname, String cardNumber, String city, String custumerId, String deviceId, String expirationMonthYear, String nameOncard, String state, String zip){
		
		
		
		//AddOrUpdateCardRequest  request = new AddOrUpdateCardRequestMC();
		cardNickname = cardNickname.substring(0,cardNickname.length()> 17 ? 17 : cardNickname.length());
		AddOrUpdateCardRequestMC request = new AddOrUpdateCardRequestMC();
		request.setAddressLine1(address);
		request.setAddressLine2("");
		request.setCardNickname(cardNickname+"-MC");//
		request.setCardNumber(cardNumber);
		request.setCity(city);
		request.setCustomerId(custumerId);
		request.setDiviceId(deviceId);
		request.setExpirationMonthYear(expirationMonthYear);
		request.setNameOnCard( nameOncard );
		request.setState(state);
		request.setZip(zip);
		
		LOGGER.debug("request AddCard: " + gson.toJson(request));
		CardsResponse response = mcIngorequest.AddOrUpdateCard(request, deviceId);
		
		return response;
		
	}
	
	public CreateCardResponse CreateCardFV(String name, String patern, String address, String state, String city, String cp, String email, String phone, String birthday, String ssn, long idUser, String govtId, String govtIdExpirationDate,String govtIdIssueDate,String govtIdIssueState){
		ServicesProxy proxy = new ServicesProxy();
		CreateCardResponse responseFV = null; 
		String json = "";
		
		String jsonResponseFV;
		try {
			

			CreateCard requestFV = new CreateCard();
			requestFV.setName(name);
			requestFV.setPatern(patern);
			requestFV.setAddress(address);
			requestFV.setState(state);
			requestFV.setCity(city);
			requestFV.setPostalCode(cp);
			requestFV.setEmail(email);
			requestFV.setPhone(phone);
			requestFV.setBirthday(birthday);
			requestFV.setSsn(ssn);
			requestFV.setIdUsuario(idUser);
			requestFV.setGovtId(govtId); //DDMMYYYY
			requestFV.setGovtIdExpirationDate(govtIdExpirationDate);
			requestFV.setGovtIdIssueDate(govtIdIssueDate);
			requestFV.setGovtIdIssueState(govtIdIssueState);
			
			json = gson.toJson(requestFV);
			LOGGER.debug("json Request FV: " +  json);
			
			jsonResponseFV = proxy.createCard(json);
			LOGGER.debug("Response FV: " + jsonResponseFV);
			responseFV = gson.fromJson(jsonResponseFV, CreateCardResponse.class);
			
		} catch (RemoteException e) {
			LOGGER.error("Error al crear Tarjeta FirstView", e);
			e.printStackTrace();
		}
		
		return responseFV;
		
	}
	
	
	private McResponse active(HashMap parameters){
		String responseJson = mapper.ProcessCard(parameters);
		McResponse activa = gson.fromJson(responseJson, McResponse.class);
		return activa;
	}
	
	private TarjetaResponse getCards(HashMap parameters, long idUsuario){
		TarjetaResponse response = new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("GetTResponse: " + responseJson);
			if(responseJson.contains("idError"))
			{
				response = gson.fromJson(responseJson, TarjetaResponse.class);
				response.setTarjetas(new ArrayList<MobileCardCard>());
			}
			else
				response = gson.fromJson("{\"MobileCardCard\":"+ responseJson + "}", TarjetaResponse.class);
			HardCardDencrypt(response,idUsuario);
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas: " +  ex.getMessage());
			response.setIdError(103);
			response.setMensajeError("Error al obtener tarjetas desde base de datos");
			return response;
		}
		
	}
	
	private void HardCardDencrypt(TarjetaResponse response, long idUsuario){
		int tam =  response.getTarjetas().size();
		String pan;
		String vig;
		String cod;
		for(int i=0; i<tam; i++)
		{
			pan = UtilsService.getSMS(response.getTarjetas().get(i).getPan());
			vig = UtilsService.getSMS(response.getTarjetas().get(i).getVigencia());
			
			response.getTarjetas().get(i).setPan(AddcelCrypto.encryptHard(pan));
			response.getTarjetas().get(i).setVigencia(AddcelCrypto.encryptHard(vig));
			
			if(response.getTarjetas().get(i).getCodigo() != null && !response.getTarjetas().get(i).getCodigo().isEmpty())
			{
				cod =  UtilsService.getSMS(response.getTarjetas().get(i).getCodigo());
				response.getTarjetas().get(i).setCodigo(AddcelCrypto.encryptHard(cod));
			}
			else
			{
				response.getTarjetas().get(i).setCodigo("");
			}
			
			if(response.getTarjetas().get(i).isMobilecard()){
				try{
					LOGGER.debug("[OBTENIENDO SALDO MOBILECARD] ["+idUsuario+"]");
					ServicesProxy proxy = new ServicesProxy();
					String amount= proxy.checkBalance(idUsuario+"");
					LOGGER.debug("[AMOUNT] : "+ amount);
					response.getTarjetas().get(i).setBalance(Double.parseDouble(amount));
				}catch(Exception ex){
					LOGGER.error("Error al obtener monto de tarjeta Mobilecard ");
					response.getTarjetas().get(i).setBalance(new Double(0));
				}
			}
			
			
		}
		
	}
	
	
	private TarjetaResponse delete(HashMap parameters){
		TarjetaResponse response =  new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			McResponse delete = gson.fromJson(responseJson, McResponse.class);
			response.setIdError(delete.getIdError());
			response.setMensajeError(delete.getMensajeError());
			LOGGER.debug("idError: " + response.getIdError() );
			LOGGER.debug("Mensage: " +  response.getMensajeError());
			return response;
		}catch(Exception ex){
			LOGGER.error("error al enviar proceso borrar tarjeta en base de datos");
			response.setIdError(104);
			response.setMensajeError("Error al procesar borrado de tarjeta");
			return response;
		}
		
	}
	
	
}
