package com.addcel.wallet.services;

import java.security.KeyStore;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.wallet.client.model.vo.MailSender;




@Service
public class EmailSenderClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderClient.class);
	private RestTemplate client;
	public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER     = new AllowAllHostnameVerifier();
	
	public EmailSenderClient() {
		// TODO Auto-generated constructor stub
		client = getRestemplate();
	}
	
	
	public boolean SendMail(MailSender email, String url){
		
		boolean send = false;
		
		try{
			HttpHeaders headers = getHeaders();
			HttpEntity<MailSender> request = new HttpEntity<MailSender>(email,headers);
			String response = client.postForObject(url, request, String.class);
			LOGGER.debug("RESPUESTA MAILSENDER: "  + response);
			send = true;
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR EMAIL", ex);
		}
		
		return send;
	}
	
	private HttpHeaders getHeaders(){
		
		try{
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			return headers;
		}catch(Exception ex){
			LOGGER.error("ERROR AL CREAR HEADER",ex);
			return null;
		}
	}
	
	private RestTemplate getRestemplate() {
		
		try{
			
			 KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			    keyStore.load(this.getClass().getResourceAsStream("KeyStore.jks"), "addcel".toCharArray());
			    LOGGER.debug("Key store cargado " + keyStore.size());
			    SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
			            new SSLContextBuilder()
			                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
			                    .loadKeyMaterial(keyStore, "addcel".toCharArray())
			                    .build(),
			                    ALLOW_ALL_HOSTNAME_VERIFIER);

			    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

			    ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			    RestTemplate restTemplate = new RestTemplate(requestFactory);
			    return restTemplate;
		}catch(Exception ex){
			LOGGER.error("Error al cargar store ", ex);
			return null;
		}
		}
	
}
