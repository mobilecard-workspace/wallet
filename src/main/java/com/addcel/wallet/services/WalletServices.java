package com.addcel.wallet.services;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.firstView.service.ServicesProxy;
import com.addcel.firstView.service.model.CreateCard;
import com.addcel.firstView.service.model.CreateCardResponse;
import com.addcel.ingo.bridge.client.model.vo.AddOrUpdateCardRequestMC;
import com.addcel.ingo.bridge.client.model.vo.CardsResponse;
import com.addcel.ingo.bridge.client.model.vo.CustomerResponse;
import com.addcel.ingo.bridge.client.model.vo.EnrollCustomerMC;
import com.addcel.ingo.bridge.client.model.vo.GetRegisteredCardsRequestMC;
import com.addcel.utils.AddcelCrypto;
import com.addcel.wallet.client.model.vo.DefaultCardReq;
import com.addcel.wallet.client.model.vo.EstadosResponse;
import com.addcel.wallet.client.model.vo.McResponse;
import com.addcel.wallet.client.model.vo.MobileCardCard;
import com.addcel.wallet.client.model.vo.MobilecardCardMovement;
import com.addcel.wallet.client.model.vo.MobilecardCardResponse;
import com.addcel.wallet.client.model.vo.ResponseSMS;
import com.addcel.wallet.client.model.vo.TarjetaRequest;
import com.addcel.wallet.client.model.vo.TarjetaRequestMC;
import com.addcel.wallet.client.model.vo.TarjetaResponse;
import com.addcel.wallet.client.model.vo.TransactionData;
import com.addcel.wallet.client.model.vo.Transactions;
import com.addcel.wallet.client.model.vo.TransactionsRes;
import com.addcel.wallet.client.model.vo.previvaleUser;
import com.addcel.wallet.client.viamericas.ProfileCardRequest;
import com.addcel.wallet.client.viamericas.ViamericasClient;
import com.addcel.wallet.mybatis.model.mapper.WalletMapper;
import com.addcel.wallet.mybatis.model.vo.AppCipher;
import com.addcel.wallet.mybatis.model.vo.Card;
import com.addcel.wallet.mybatis.model.vo.CardCommerce;
import com.addcel.wallet.mybatis.model.vo.CardImage;
import com.addcel.wallet.mybatis.model.vo.Commerce;
import com.addcel.wallet.mybatis.model.vo.Estados;
import com.addcel.wallet.mybatis.model.vo.Login;
import com.addcel.wallet.mybatis.model.vo.Movements;
import com.addcel.wallet.mybatis.model.vo.PrevivaleCard;
import com.addcel.wallet.mybatis.model.vo.PrevivaleStock;
import com.addcel.wallet.mybatis.model.vo.ProjectMC;
import com.addcel.wallet.mybatis.model.vo.User;
import com.addcel.wallet.utils.Constants;
import com.addcel.wallet.utils.UtilsMC;
import com.google.gson.Gson;
import com.sun.xml.bind.v2.schemagen.xmlschema.Appinfo;

@Service
public class WalletServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WalletServices.class);
	@Autowired
	private WalletMapper mapper;
	
	@Autowired
	ViamericasClient viamericasClient;
	
	@Autowired
	private MCIngorequest mcIngorequest;
	
	private Gson gson = new Gson();
	
	private static final String PREVIVALE_URL_REGISTRO = "http://localhost/PreviVale/api/registrarCliente";
	
	
	public TarjetaResponse addCard(TarjetaRequest cardRequest, int idApp){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE INSERCION DE TARJETA {} ", cardRequest);
			AppCipher appCipher = mapper.getAppCipher(idApp);
			
			Login login = mapper.getLogin(cardRequest.getIdUsuario(), idApp); 
			String pan;
			String vig;
			String cod;
			
			if(idApp < 3){
				 pan =  AddcelCrypto.decryptHard(cardRequest.getPan());
				 vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
				 cod = cardRequest.getCodigo() == null ? "" : AddcelCrypto.decryptHard(cardRequest.getCodigo());
			}else{
				if(appCipher != null){
					if(appCipher.getName().equalsIgnoreCase("claro360")){
						pan = new String(Base64.getDecoder().decode(cardRequest.getPan()),"utf-8");
						vig = new String(Base64.getDecoder().decode(cardRequest.getVigencia()),"utf-8");;
						cod = new String(Base64.getDecoder().decode(cardRequest.getCodigo()),"utf-8");
					}else{
						pan = UtilsService.decryptApp(appCipher.getApi_key(), cardRequest.getPan());
						vig = UtilsService.decryptApp(appCipher.getApi_key(), cardRequest.getVigencia());
						cod = UtilsService.decryptApp(appCipher.getApi_key(), cardRequest.getCodigo());
					}
				}else{
					 response.setIdError(600);
					 response.setMensajeError("ERROR AL MOMENTO DE DESCIFRAR DATOS SENCIBLES, CONTACTE A SOPORTE");
					 return response;
				}
			}
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", cardRequest.getIdUsuario());
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", pan);
			parameters.put("nnumtarjetacrip", UtilsService.setSMS(pan));
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 1);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex() );
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("mobilecard", cardRequest.isMobilecard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
			
			
			LOGGER.debug("parameters add card: " + parameters.get("nproceso") );
			
			String responseAdd = mapper.ProcessCard(parameters);
			McResponse add_response = gson.fromJson(responseAdd, McResponse.class);
			
			LOGGER.debug("Respuesta add: " + responseAdd);	
			parameters.replace("nproceso", 3);
			
			response = getCards(parameters, cardRequest.getIdUsuario(), idApp, login.getIdpais(), cardRequest.getIdioma());
			response.setIdError(add_response.getIdError());
			response.setMensajeError(add_response.getMensajeError());
			
			
			//VIAMERICAS SOLO SI ES USA
			if(login.getIdpais() == 3)
				CreateProfileViamericas(cardRequest.getDomAmex(), cardRequest.getNombre().split(" ")[0], cardRequest.getNombre().split(" ")[1], pan, cod, vig.replace("/", ""), login.getId_sender(), cardRequest.getIdUsuario(), login.getUser().split("@")[0], "K");
			
			
			LOGGER.debug("FINALIZANDO PROCESO DE INSERCION DE TRAJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir tarjeta : " + ex.getMessage());
			LOGGER.error("Error:", ex);
			return LogService.getAddMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}
	
	private void CreateProfileViamericas(String address, String cardFirstName, String cardLastName, String cardNumber, String cvv, String expDate, String idSender, long idUsuario, String nickName, String paymentType){
		try{
			if(idSender != null && !idSender.isEmpty())
			{
				ProfileCardRequest request = new ProfileCardRequest();
				request.setAddress(address);
				request.setCardFirstName(cardFirstName);
				request.setCardLastName(cardLastName);
				request.setCardNumber(cardNumber);
				request.setCvv(cvv);
				request.setExpDate(expDate);
				request.setIdSender(idSender);
				request.setIdUsuario(idUsuario);
				request.setNickName(nickName);
				request.setPaymentType(paymentType);
			
				viamericasClient.createSenderProfileCard(request);
			}
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL CREAR PROFILE VIAMERICAS]", ex);
		}
	}
	
	public TarjetaResponse AddMobileCardCard2(TarjetaRequestMC carRequest, int idApp){
		TarjetaResponse response =  new TarjetaResponse();
		CustomerResponse responseIngo = new CustomerResponse();
		User user = null;
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy"); 
		String expira = "";
		String anio = "";
		String mes = "";
		Card mobilecardCard = null;
		CreateCardResponse responseFV = new CreateCardResponse();
		GetRegisteredCardsRequestMC resgisterCards = new GetRegisteredCardsRequestMC();
		int cards = 0;
		
		try{
			user= mapper.getUser(carRequest.getIdUsuario(), idApp);
			user.setUsr_direccion(carRequest.getDireccion());
			user.setUsr_ciudad(carRequest.getCiudad());
			user.setUsr_cp(carRequest.getCp());
			user.setUsr_fecha_nac(new java.sql.Date(sdf.parse(carRequest.getFechaNac()).getTime()));
			user.setUsr_nss(carRequest.getSsn());
			user.setUsr_id_estado(mapper.getState(carRequest.getEstado(), -1).getId());
			
			//enrrolar usuario en ingo
			if(user.getId_ingo() == null || user.getId_ingo().isEmpty() ){	
				responseIngo = EnrollCustomer(carRequest.getDireccion(), carRequest.getCiudad(),carRequest.getFechaNac().substring(4,8)+"-"+carRequest.getFechaNac().substring(2,4)+"-"+carRequest.getFechaNac().substring(0,2),user.geteMail(), user.getUsr_nombre(), user.getUsr_apellido(), user.getUsr_telefono(), carRequest.getSsn(), carRequest.getEstado(),carRequest.getCp(),user.getImei());
				user.setId_ingo(responseIngo.getCustomerId());
				user.setId_aplicacion(idApp);
				mapper.updateCustomerId(user);
			}
			else
			{
				LOGGER.debug("Usuario ya se encuentra en INGO... [RecuPerando Datos]");
				
				responseIngo.setCustomerId(user.getId_ingo());
				mobilecardCard = mapper.getMobilecardCard(user.getId_usuario(), idApp);
			}
			
			 
			//Checar si tiene tajetas en ingo
			resgisterCards.setCustomerId(responseIngo.getCustomerId());
			resgisterCards.setDeviceId(user.getImei());
			//CardsResponse cards_response =  mcIngorequest.GetRegisteredCards(resgisterCards, resgisterCards.getDeviceId());
			cards = 0;//cards_response.getCards().size();
			//validando que no tenga registrada tarjeta MC en ingo
			if(cards == 0 )
			{
					//crear tarjeta FV
					if(mobilecardCard == null || mobilecardCard.getPan() == null )
						responseFV = CreateCardFV(user.getUsr_nombre(),user.getUsr_apellido(),carRequest.getDireccion(),carRequest.getEstado(),carRequest.getCiudad(),carRequest.getCp(), user.geteMail(),user.getUsr_telefono(),carRequest.getFechaNac().substring(2,4) + carRequest.getFechaNac().substring(0,2) + carRequest.getFechaNac().substring(4,carRequest.getFechaNac().length()),carRequest.getSsn(),carRequest.getIdUsuario(),carRequest.getGovtId(), carRequest.getGovtIdExpirationDate(),carRequest.getGovtIdIssueDate(),carRequest.getGovtIdIssueState());
					else
					{
						//Recuperar datos de tarjeta del wallet
						responseFV.setCardNumber(UtilsService.getSMS(mobilecardCard.getPan()));
						responseFV.setCvv(UtilsService.getSMS(mobilecardCard.getCodigo()));
						responseFV.setErrorCode(0);
						responseFV.setErrorDescription("");
						responseFV.setExpirationDate(UtilsService.getSMS(mobilecardCard.getVigencia()));
						responseFV.setIdUsuario(user.getId_usuario());
						
					}
					
					if(responseFV.getErrorCode() == 0 && responseFV.getCardNumber()!=null && responseFV.getCvv()!= null && responseFV.getExpirationDate() != null){
						
						 mes = responseFV.getExpirationDate().substring(0,2);
						 anio = responseFV.getExpirationDate().substring(responseFV.getExpirationDate().length()-2);
						 expira = mes+anio; //MMYY
						
						if(responseIngo.getCustomerId() != null && !responseIngo.getCustomerId().isEmpty())
						{
							//agregar targeta a Ingo
							resgisterCards.setCustomerId(responseIngo.getCustomerId());
							resgisterCards.setDeviceId(user.getImei());
							
							//
					        CardsResponse responseCardIngo = addCardIngo(user.getUsr_direccion(), user.getUsr_nombre(), responseFV.getCardNumber(), carRequest.getCiudad(), responseIngo.getCustomerId(), user.getImei(),expira , (user.getUsr_nombre().trim() + " " + user.getUsr_apellido()).trim() , carRequest.getEstado(), user.getUsr_cp());
					        
					        if(responseCardIngo.getErrorCode() == 0){
								LOGGER.debug("Se Agrego correctamente la tarjeta al usuario {}",responseIngo.getCustomerId() );
							}
							else
							{
								LOGGER.debug("Error al agregar tarjeta al usuario {} message {} ", responseIngo.getCustomerId(), responseCardIngo.getErrorMessage());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
					        
						}else
						{
							LOGGER.debug("Error al enrrolar usuario en ingo {} {}", carRequest.getIdUsuario(), responseIngo.getErrorMessage());
							response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
						}
						
						
						//agregar Tarjeta al wallet
						if(mobilecardCard == null || mobilecardCard.getPan() == null)
						{
							TarjetaRequest cardRequest = new TarjetaRequest();
							//cardRequest.setCodigo(responseFV.getCvv());
							cardRequest.setCpAmex("");
							cardRequest.setDeterminada(false);
							cardRequest.setDomAmex("");
							cardRequest.setIdioma(carRequest.getIdioma());
							cardRequest.setIdTarjeta(0);
							cardRequest.setIdUsuario(user.getId_usuario());
							cardRequest.setMobilecard(true);
							cardRequest.setNombre(user.getUsr_nombre().toString() + " " + user.getUsr_apellido().trim());
							cardRequest.setPan(AddcelCrypto.encryptHard(responseFV.getCardNumber()));
							cardRequest.setTipo(0);
							cardRequest.setVigencia(AddcelCrypto.encryptHard(mes+"/"+anio));
							cardRequest.setCodigo(AddcelCrypto.encryptHard(responseFV.getCvv()));
							cardRequest.setTipoTarjeta(Constants.CREDIT_CARD_TYPE);
							
							response = addCard(cardRequest, idApp);
							
							if(response.getIdError() == 0){
								LOGGER.debug("Se agrego la Tarjeta correctamente al Wallet");
								response.setMensajeError(response.getMensajeError());
								ProjectMC project =  mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
								//enviando email de confirmacion
								if(carRequest.getIdioma().equalsIgnoreCase("es"))
									UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_MOBILECARD_CARD_ES"), mapper.getParameter("@ASUNTO_MOBILECARD_CARD_ES"), "", "", UtilsMC.maskCard(responseFV.getCardNumber(), "X"), cardRequest.getNombre(), new String[]{Constants.Path_images+"user_mobilecard_card.PNG"},project.getUrl());
								else
									UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MENSAJE_MOBILECARD_CARD_EN"), mapper.getParameter("@ASUNTO_MOBILECARD_CARD_EN"), "", "", UtilsMC.maskCard(responseFV.getCardNumber(), "X"), cardRequest.getNombre(), new String[]{Constants.Path_images+"user_mobilecard_card.PNG"},project.getUrl());
								
							}
							else{
								LOGGER.debug("Error al agregar tarejta al wallet {} {}", response.getMensajeError(), cardRequest.getIdUsuario());
								response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
							}
						}else
							LOGGER.debug("LA TARJETA YA SE ENCONTRABA EN EL WALLET...");
						
						
					}else{
						LOGGER.debug("Error al crear Targeta FirstView {} {} " + carRequest.getIdUsuario(), responseFV.getErrorDescription());
						response = LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2); //LogService.ErrorFV(carRequest.getIdioma().equals("es") ? 1 : 2);
					}
			}
			else
			{
				LOGGER.debug("Se detecto un error con una tarjeta registrada previamente, contacte al administrador");
				response.setIdError(110);
				response.setMensajeError("An error was detected with a previously registered card, contact technical support"); //Se detecto un error con una tarjeta registrada previamente, contacte a soporte tecnico
				response.setTarjetas(new ArrayList<MobileCardCard>());
			}
			
		}catch(Exception ex){
			LOGGER.error("Error al crear tarjetaMobileCard", ex);
			
			return LogService.getCardsMessageerror(carRequest.getIdioma().equals("es") ? 1 : 2);
		}
		return response;
		
	}
	
	public TarjetaResponse update(TarjetaRequest cardRequest, int idApp){
		TarjetaResponse response= new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE ACTUALIZACION");
			//Login login = mapper.getLogin(cardRequest.getIdUsuario());
			HashMap parameters = new HashMap<>();
			String vig = "";
			String cod = "";
			if(idApp < 4)
			{
				vig =  AddcelCrypto.decryptHard(cardRequest.getVigencia());
				cod =  AddcelCrypto.decryptHard(cardRequest.getCodigo());
			}else{
				AppCipher appInfo = mapper.getAppCipher(idApp);
				if(appInfo.getName().equalsIgnoreCase("claro360")){
					vig = new String(Base64.getDecoder().decode(cardRequest.getVigencia()), "utf-8");
					cod = new String(Base64.getDecoder().decode(cardRequest.getCodigo()), "utf-8");
				}else{
					vig = UtilsService.decryptApp(appInfo.getApi_key(), cardRequest.getVigencia());
					cod = UtilsService.decryptApp(appInfo.getApi_key(), cardRequest.getCodigo());
				}
			}
			
			parameters.put("nusuario", cardRequest.getIdUsuario());
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", UtilsService.setSMS(vig));
			parameters.put("nproceso", 5);
			parameters.put("nidtarjetausuario", cardRequest.getIdTarjeta());
			parameters.put("nct", UtilsService.setSMS(cod));
			parameters.put("ntarjeta", cardRequest.getNombre());
			parameters.put("domamex",cardRequest.getDomAmex());
			parameters.put("cpamex", cardRequest.getCpAmex());
			parameters.put("idioma", cardRequest.getIdioma());
			parameters.put("mobilecard", cardRequest.isMobilecard() ? 1 : 0);
			parameters.put("tipotarjeta", cardRequest.getTipoTarjeta() == null ? Constants.CREDIT_CARD_TYPE : cardRequest.getTipoTarjeta().toUpperCase());
			
			//LOGGER.debug("IsMobileCard: " + cardRequest.isMobilecard());
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("Campos actualizados... " + responseJson);
			
			McResponse mcResponseUpdate = gson.fromJson(responseJson, McResponse.class);
			
			
			if(mcResponseUpdate.getIdError() == 0 && cardRequest.isDeterminada())
			{
				parameters.replace("nproceso", 2);
				McResponse mcResponse = active(parameters);
				LOGGER.debug("activando");
				if(mcResponse.getIdError()!= 0){
					response.setIdError(mcResponse.getIdError());
					response.setMensajeError(mcResponse.getMensajeError());
				}
				
			}
			
			parameters.put("nidtarjetausuario", 1); ///para saber si es de mexico la peticion
			parameters.replace("nproceso", 3);
			LOGGER.debug("cards obtenidas...");
		//	responseJson = getCards(parameters);
			
			response = getCards(parameters, cardRequest.getIdUsuario(), idApp, 1, cardRequest.getIdioma());//gson.fromJson(responseJson,TarjetaResponse.class);
			response.setIdError(mcResponseUpdate.getIdError());
			response.setMensajeError(mcResponseUpdate.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE ACTUALIZACION");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al añadir actualizar tarjeta : " + ex.getMessage());
			response.setIdError(101);
			response.setMensajeError("error al actualizar tarjeta");
			return LogService.CardUpdateMessageError(cardRequest.getIdioma().equalsIgnoreCase("es") ? 1 : 2);
		}
		
		
	}
	
	private McResponse active(HashMap parameters){
		String responseJson = mapper.ProcessCard(parameters);
		McResponse activa = gson.fromJson(responseJson, McResponse.class);
		return activa;
	}
	
	public TarjetaResponse delete(int idTarjeta, long idUsuario, String idioma, int idApp){
		TarjetaResponse response = new TarjetaResponse();
		try{
			LOGGER.debug("INICIANDO PROCESO DE BORRADO DE TARJETA");
			//Login login = mapper.getLogin(idUsuario); 
			
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", idUsuario);
			parameters.put("idapp", idApp);
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 4);
			parameters.put("nidtarjetausuario", idTarjeta);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", "");
			TarjetaResponse responseDelete = delete(parameters);
			
			parameters.replace("nproceso", 3);
			parameters.replace("nidtarjetausuario", 1);
			
			response = getCards(parameters, idUsuario, idApp, 1, idioma);
			
			response.setIdError(responseDelete.getIdError());
			response.setMensajeError(responseDelete.getMensajeError());
			
			LOGGER.debug("FINALIZANDO PROCESO DE BORRADO DE TARJETA");
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : " + ex.getMessage());
			
			return LogService.CardDeleteMessageError(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public TarjetaResponse WithoutMovement(long idUsuario, String idioma, int idApp, int process, int type){
		try{
			LOGGER.debug("INICIANDO PROCESO GETCARDS sin movimientos");
			//Login login = mapper.getLogin(idUsuario); 
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", idUsuario);
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", process);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", type+"");
			LOGGER.debug("fINALIZANDO PROCESO GETCARDS");
			return getCards(parameters, idUsuario, idApp, 1, idioma);//(parameters,idUsuario);
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : ", ex);
			return LogService.getCardsMessageerror(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public TarjetaResponse getCards(long idUsuario, String idioma, int idApp){
		try{
			LOGGER.debug("INICIANDO PROCESO GETCARDS");
			//Login login = mapper.getLogin(idUsuario); 
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", idUsuario);
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 3);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", "");
			LOGGER.debug("fINALIZANDO PROCESO GETCARDS");
			return getCards(parameters, idUsuario, idApp, 1, idioma);
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : ", ex);
			return LogService.getCardsMessageerror(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	public TarjetaResponse WithoutMovement(long idUsuario, String idioma, int idApp){
		try{
			LOGGER.debug("INICIANDO PROCESO GETCARDS");
			//Login login = mapper.getLogin(idUsuario); 
			HashMap parameters = new HashMap<>();
			parameters.put("nusuario", idUsuario);
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 6);
			parameters.put("nidtarjetausuario", 1);
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","" );
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta", "");
			LOGGER.debug("fINALIZANDO PROCESO GETCARDS");
			return getCards(parameters, idUsuario, idApp, 1, idioma);
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas : ", ex);
			return LogService.getCardsMessageerror(idioma.equalsIgnoreCase("es") ? 1 : 2);
		}
		
	}
	
	private TarjetaResponse getCards(HashMap parameters, long idUsuario, int idApp, int idPais,String idioma){
		TarjetaResponse response = new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			LOGGER.debug("GetTResponse: " + responseJson);
			if(responseJson.contains("idError"))
			{
				response = gson.fromJson(responseJson, TarjetaResponse.class);
				response.setTarjetas(new ArrayList<MobileCardCard>());
			}
			else
				response = gson.fromJson("{\"MobileCardCard\":"+ responseJson + "}", TarjetaResponse.class);
			HardCardDencrypt(response,idUsuario,idApp,idPais,idioma);
			return response;
		}catch(Exception ex){
			LOGGER.error("Error al obtener tarjetas: " +  ex.getMessage(), ex);
			response.setIdError(103);
			response.setMensajeError("Error al obtener tarjetas desde base de datos");
			return response;
		}
		
	}
	
	private TarjetaResponse delete(HashMap parameters){
		TarjetaResponse response =  new TarjetaResponse();
		try{
			String responseJson = mapper.ProcessCard(parameters);
			McResponse delete = gson.fromJson(responseJson, McResponse.class);
			response.setIdError(delete.getIdError());
			response.setMensajeError(delete.getMensajeError());
			LOGGER.debug("idError: " + response.getIdError() );
			LOGGER.debug("Mensage: " +  response.getMensajeError());
			return response;
		}catch(Exception ex){
			LOGGER.error("error al enviar proceso borrar tarjeta en base de datos", ex);
			response.setIdError(104);
			response.setMensajeError("Error al procesar borrado de tarjeta");
			return response;
		}
		
	}
	
	private void HardCardDencrypt(TarjetaResponse response, long idUsuario, int idApp, int idPais, String idioma){
		int tam =  response.getTarjetas().size();
		String pan;
		String vig;
		String cod;
		AppCipher appCipher = mapper.getAppCipher(idApp);
		Integer hasMC = mapper.hasMobilecard(idUsuario,idApp);
		response.setHasMobilecard(hasMC > 0 ? true : false);
		
		
		for(int i=0; i<tam; i++)
		{
			
			pan = UtilsService.getSMS(response.getTarjetas().get(i).getPan());
			vig = UtilsService.getSMS(response.getTarjetas().get(i).getVigencia());
			
			if(response.getTarjetas().get(i).isPrevivale()){
				LOGGER.debug("OBTENIENDO BALANCE PREVIVALE");
				response.getTarjetas().get(i).setBalance(0.0);
				LOGGER.info("OBTIENENDO CUENTA CLABE ASIGNADA TARJETA: {}", response.getTarjetas().get(i).getIdTarjeta());
				response.getTarjetas().get(i).setClabe(mapper.getCuentaClabeMobilecardByIdTarjeta(response.getTarjetas().get(i).getIdTarjeta()));
				/*PreviValeService previvale = new PreviValeService();
				ResponseSMS balance = previvale.getBalance(idUsuario, idApp, idPais, idioma, response.getTarjetas().get(i).getPan());
				try{
					response.getTarjetas().get(i).setBalance( balance == null ? 0.0 : Double.parseDouble(balance.getData().getAuthorization()));
					
					LOGGER.info("OBTIENENDO CUENTA CLABE ASIGNADA TARJETA: {}", response.getTarjetas().get(i).getIdTarjeta());
					response.getTarjetas().get(i).setClabe(mapper.getCuentaClabeMobilecardByIdTarjeta(response.getTarjetas().get(i).getIdTarjeta()));
					LOGGER.info("CUENTA CLABE ASIGNADA : {}", response.getTarjetas().get(i).getClabe());
				}catch(Exception e){
					LOGGER.debug("error al obtener saldo... ", e);
					response.getTarjetas().get(i).setBalance(0.0);
				}*/
			}
			
			if(idApp < 4){
				response.getTarjetas().get(i).setPan(AddcelCrypto.encryptHard(pan));
				response.getTarjetas().get(i).setVigencia(AddcelCrypto.encryptHard(vig));
			}else{
				if(appCipher != null){
					if(appCipher.getName().equalsIgnoreCase("claro360")){
						try {
							response.getTarjetas().get(i).setPan( new String(Base64.getEncoder().encodeToString(pan.getBytes("utf-8"))) );
							response.getTarjetas().get(i).setVigencia( new String(Base64.getEncoder().encodeToString(vig.getBytes("utf-8"))) );
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else{
						response.getTarjetas().get(i).setPan(UtilsService.encryptApp(appCipher.getApi_key(), pan));
						response.getTarjetas().get(i).setVigencia(UtilsService.encryptApp(appCipher.getApi_key(), vig));
					}
					
				}else
				{
					response.getTarjetas().get(i).setPan("");
					response.getTarjetas().get(i).setVigencia("");
				}
			}
			
			if(response.getTarjetas().get(i).getMovements() == null)
				response.getTarjetas().get(i).setMovements(new ArrayList<MobilecardCardMovement>());
			
			if(response.getTarjetas().get(i).getCodigo() != null && !response.getTarjetas().get(i).getCodigo().isEmpty())
			{
				cod =  UtilsService.getSMS(response.getTarjetas().get(i).getCodigo());
				
				if(idApp < 4)
					response.getTarjetas().get(i).setCodigo(AddcelCrypto.encryptHard(cod));
				else{
					if(appCipher != null){
						if(appCipher.getName().equalsIgnoreCase("claro360"))
							try {
								response.getTarjetas().get(i).setCodigo(new String(Base64.getEncoder().encodeToString(cod.getBytes("utf-8"))) );
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						else
							response.getTarjetas().get(i).setCodigo(UtilsService.encryptApp(appCipher.getApi_key(), cod));
					}else
						response.getTarjetas().get(i).setCodigo("");
				}
			}
			else
			{
				response.getTarjetas().get(i).setCodigo("");
			}
			
			if(response.getTarjetas().get(i).isMobilecard()){
				response.getTarjetas().get(i).setBalance(new Double(0));
				/*try{
					LOGGER.debug("[OBTENIENDO SALDO MOBILECARD] ["+idUsuario+"]");
					ServicesProxy proxy = new ServicesProxy();
					String amount= proxy.checkBalance(idUsuario+"");
					LOGGER.debug("[AMOUNT] : "+ amount);
					response.getTarjetas().get(i).setBalance(Double.parseDouble(amount));
				}catch(Exception ex){
					LOGGER.error("Error al obtener monto de tarjeta Mobilecard ",ex);
					response.getTarjetas().get(i).setBalance(new Double(0));
				}*/
			}
			
			
			////*********buscar imagenes*****///
			CardImage cImage = mapper.getCardImage(pan.substring(0, 6));
			if(cImage != null){
				response.getTarjetas().get(i).setImg_full(cImage.getUrl_imagen_larga());
				response.getTarjetas().get(i).setImg_short(cImage.getUrl_imagen_corta());
			}else
			{
				if(response.getTarjetas().get(i).getTipo().equals("VISA")){
					response.getTarjetas().get(i).setImg_full("http://199.231.160.203/images/cards/full/Visa.png");
					response.getTarjetas().get(i).setImg_short("http://199.231.160.203/images/cards/short/Visa.png");
				}else if(response.getTarjetas().get(i).getTipo().equals("MasterCard")){
					response.getTarjetas().get(i).setImg_full("http://199.231.160.203/images/cards/full/Mastercard.png");
					response.getTarjetas().get(i).setImg_short("http://199.231.160.203/images/cards/short/Mastercard.png");
				}else if(response.getTarjetas().get(i).getTipo().equals("AmEx")){
					response.getTarjetas().get(i).setImg_full("http://199.231.160.203/completas/Amex.png");
					response.getTarjetas().get(i).setImg_short("http://199.231.160.203/cortadas/Amex.png");
				}else {
					response.getTarjetas().get(i).setImg_full("http://199.231.160.203/images/cards/full/Mobilecard.png");
					response.getTarjetas().get(i).setImg_short("http://199.231.160.203/images/cards/short/Mobilecard.png");
				}
			}
			
		}
		
	}
	
private CustomerResponse EnrollCustomer(String address, String city, String dateOfBirth, String email, String firstName, String lastName, String mobileNumber, String ssn, String state, String zip, String deviceId){
		
		EnrollCustomerMC request = new EnrollCustomerMC();
		//EnrollCustomerRequest request = new EnrollCustomerRequest();
		CustomerResponse response = null;
			
		request.setAddressLine1(address);
		request.setAddressLine2("");
		request.setAllowTexts(true);
		request.setCity(city);
		request.setCountryOfOrigin(""); //optional 
		request.setDateOfBirth(dateOfBirth); //yyyy-MM-dd (e.g., 1978-09-18) 1994-07-02 00:00:00 
		request.setEmail(email);
		request.setFirstName(firstName);
		request.setGender(""); //Optional Single character string indicating M/F.
		request.setHomeNumber("");
		request.setLastName(lastName);
		request.setMiddleInitial(""); //Optional Single character middle initial. Max length is 1 character.
		request.setMobileNumber(mobileNumber);
		request.setSsn(ssn);
		request.setState(state);
		request.setSuffix(""); // optional Typically would contain one of Jr, Sr, etc.
		request.setTitle(""); // optional Typically would contain one of Mrs, Mr, etc.
		request.setZip(zip);
		request.setDeviceId(deviceId);
		
		
		response =  mcIngorequest.EnrollCustomer(request, deviceId);
		
		return response;
	}

public CreateCardResponse CreateCardFV(String name, String patern, String address, String state, String city, String cp, String email, String phone, String birthday, String ssn, long idUser, String govtId, String govtIdExpirationDate,String govtIdIssueDate,String govtIdIssueState){
	ServicesProxy proxy = new ServicesProxy();
	CreateCardResponse responseFV = null; 
	String json = "";
	
	String jsonResponseFV;
	try {
		CreateCard requestFV = new CreateCard();
		requestFV.setName(name);
		requestFV.setPatern(patern);
		requestFV.setAddress(address);
		requestFV.setState(state);
		requestFV.setCity(city);
		requestFV.setPostalCode(cp);
		requestFV.setEmail(email);
		requestFV.setPhone(phone);
		requestFV.setBirthday(birthday);
		requestFV.setSsn(ssn);
		requestFV.setIdUsuario(idUser);
		requestFV.setGovtId(govtId); //DDMMYYYY
		requestFV.setGovtIdExpirationDate(govtIdExpirationDate);
		requestFV.setGovtIdIssueDate(govtIdIssueDate);
		requestFV.setGovtIdIssueState(govtIdIssueState);
		
		json = gson.toJson(requestFV);
		LOGGER.debug("json Request FV: " +  json);
		
		jsonResponseFV = proxy.createCard(json);
		LOGGER.debug("Response FV: " + jsonResponseFV);
		responseFV = gson.fromJson(jsonResponseFV, CreateCardResponse.class);
		
	} catch (RemoteException e) {
		LOGGER.error("Error al crear Tarjeta FirstView", e);
		e.printStackTrace();
	}
	
	return responseFV;
	
}

private CardsResponse addCardIngo(String address, String cardNickname, String cardNumber, String city, String custumerId, String deviceId, String expirationMonthYear, String nameOncard, String state, String zip){
	
	
	
	//AddOrUpdateCardRequest  request = new AddOrUpdateCardRequestMC();
	cardNickname = cardNickname.substring(0,cardNickname.length()> 17 ? 17 : cardNickname.length());
	AddOrUpdateCardRequestMC request = new AddOrUpdateCardRequestMC();
	request.setAddressLine1(address);
	request.setAddressLine2("");
	request.setCardNickname(cardNickname+"-MC");//
	request.setCardNumber(cardNumber);
	request.setCity(city);
	request.setCustomerId(custumerId);
	request.setDiviceId(deviceId);
	request.setExpirationMonthYear(expirationMonthYear);
	request.setNameOnCard( nameOncard );
	request.setState(state);
	request.setZip(zip);
	
	LOGGER.debug("request AddCard: " + gson.toJson(request));
	CardsResponse response = mcIngorequest.AddOrUpdateCard(request, deviceId);
	
	return response;
	
}

public EstadosResponse getEstates(int idpais, int idApp)
{
	List<Estados> estados = mapper.getEstates(idpais);
	
	EstadosResponse response = new EstadosResponse();
	response.setEstados(estados);
	
	
	if(estados != null && estados.size() > 0)
	{
		response.setIdError(0);
		response.setMensajeError("");
	}
	else
	{
		response.setIdError(-1);
		if(idpais == 1)
			response.setMensajeError("País sin estados en catálogo");
		else
			response.setMensajeError("Country without states in catalog");
	}
	
	
	LOGGER.debug("Estados Count: " + estados.size());
	
	return response;
}

public Movements GetMovements(long idUsuario,String idioma, int idApp){
	
	Movements movements = new Movements();
	try{
		
		String json =  mapper.getMovements(idUsuario, idApp);
		movements = gson.fromJson(json, Movements.class);
		movements.setIdError(0);
		movements.setMensajeError("");
		
	}catch(Exception ex)
	{
		LOGGER.error("Error: ", ex);
		movements.setIdError(99);
		if(!idioma.equals("en"))
			movements.setMensajeError("Error en base de datos");
		else
			movements.setMensajeError("DataBase error");
	}
	
	
	return movements;
}

	public MobilecardCardResponse getMobilecard(long idUsuario, String idioma, int pais, int idApp) {
		MobilecardCardResponse card = new MobilecardCardResponse();
		PreviValeService previvale = new PreviValeService();
		card.setIdError(100);
		if(idioma.equalsIgnoreCase("es"))
			card.setMensajeError("No hay tarjeta mobilecard");
		else
			card.setMensajeError("No MOBILECARD Card");
		try {
			LOGGER.debug("[INICIANDO PROCESO DE OBTENCION DE TARJETA MOBILECARD]");
			TarjetaResponse cardResponse = getCards(idUsuario, idioma, idApp);
			
			int size = cardResponse.getTarjetas().size();
			for (int i = 0; i < size; i++) {
				if (pais == 3) {
					if (cardResponse.getTarjetas().get(i).isMobilecard()) {
						card.setCard(cardResponse.getTarjetas().get(i));
						card.setIdError(cardResponse.getIdError());
						card.setMensajeError(cardResponse.getMensajeError());
						break;
					}
				} else if (pais == 1) {
					PrevivaleStock  stok = mapper.GetPrevivaleCardStockByCard(UtilsService.setSMS( AddcelCrypto.decryptHard( cardResponse.getTarjetas().get(i).getPan())));
					if (cardResponse.getTarjetas().get(i).isPrevivale() || (stok != null && !stok.getNumero().isEmpty())) {
						card.setCard(cardResponse.getTarjetas().get(i));
						card.setIdError(cardResponse.getIdError());
						card.setMensajeError(cardResponse.getMensajeError());
						ResponseSMS cardBalance = previvale.getBalance(idUsuario, idApp, pais, idioma, UtilsService.setSMS( AddcelCrypto.decryptHard( cardResponse.getTarjetas().get(i).getPan())));
						try{
							card.getCard().setBalance(cardBalance == null ? 0.0 : Double.parseDouble(cardBalance.getData().getAuthorization()));
							LOGGER.info("OBTIENENDO CUENTA CLABE ASIGNADA TARJETA: {}", cardResponse.getTarjetas().get(i).getIdTarjeta());
							card.getCard().setClabe(mapper.getCuentaClabeMobilecardByIdTarjeta(cardResponse.getTarjetas().get(i).getIdTarjeta()));
							LOGGER.info("CUENTA CLABE ASIGNADA : {}", card.getCard().getClabe());
						}catch(Exception e){
							LOGGER.debug("Error al obtener balance ", e);
							card.getCard().setBalance(0.0);
						}
						break;
					}/*else{
						PrevivaleStock  stok = mapper.GetPrevivaleCardStockByCard(UtilsService.setSMS( AddcelCrypto.decryptHard( cardResponse.getTarjetas().get(i).getPan())));
						if(stok != null && !stok.getNumero().isEmpty()){
							card.setCard(cardResponse.getTarjetas().get(i));
							card.setIdError(cardResponse.getIdError());
							card.setMensajeError(cardResponse.getMensajeError());
							ResponseSMS cardBalance = previvale.getBalance(idUsuario, idApp, pais, idioma, UtilsService.setSMS( AddcelCrypto.decryptHard( cardResponse.getTarjetas().get(i).getPan())));
							card.getCard().setBalance(cardBalance == null ? 0.0 : Double.parseDouble(cardBalance.getData().getAuthorization()));
							break;
						}
					}*/
				}
			}
		} catch (Exception ex) {
			LOGGER.error("[ERROR AL OBTENER TARJETA MOBILECARD]", ex);
			card.setIdError(99);
			card.setMensajeError("General Error, try again");
		}
		return card;
	}

	public MobilecardCardResponse getPrevivaleCard(String idioma, int idPais, int idApp, User user) {
		PrevivaleCard previvaleCard = new PrevivaleCard();
		MobilecardCardResponse response = new MobilecardCardResponse();
		ResponseEntity<String> responseEntity = null;
		Integer idCardPrevivaleStock = 0;
		LOGGER.debug("request recibido: " + gson.toJson(user));
		try {
			LOGGER.info("REGISTRANDO USUARIO PARA SOLICITAR PREVIVALE - {}", user.getId_usuario());
			mapper.updateUser(user);
			idCardPrevivaleStock = mapper.getValidPrevivaleCard(user.getId_usuario());
			if(idCardPrevivaleStock == 0) {
				response.setIdError(-1);
				response.setMensajeError("No hay tarjetas en almacen.");
			} else {
				previvaleCard.setIdTarjeta(idCardPrevivaleStock);
				previvaleCard.setIdUsuario(user.getId_usuario());
				LOGGER.info("ACTUALIZANDO USUARIO - {}", user.getId_usuario());
				LOGGER.info("SOLICITANDO REGISTRO CON PREVIVALE - {}", user.getId_usuario());
				RestTemplate rest = new RestTemplate();
				HttpHeaders headers = new HttpHeaders();
				headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
				headers.set("Authorization", "Basic bW9iaWxlY2FyZG14OjgyMDA3ZWI0MjM4MjA1Yzc1ZWJjZGVmYzA2YTAxMzEx");
				HttpEntity<PrevivaleCard> entity = new HttpEntity<PrevivaleCard>(previvaleCard, headers);
				
				responseEntity = rest.exchange(PREVIVALE_URL_REGISTRO, HttpMethod.POST, entity, String.class);
//				previvaleResp = rest.postForEntity(PREVIVALE_URL_REGISTRO, previvaleCard, McResponse.class);
				LOGGER.info("RESPUESTA DE CREACION DE TARJETA PREVIVALE - {} - {}", responseEntity.getBody());
				response = gson.fromJson(responseEntity.getBody(), MobilecardCardResponse.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	public MobilecardCardResponse registrarTarjetaPrevivale(previvaleUser user, int idPais, int idApp, String idioma, String tipo, long id){
		 	
			LOGGER.debug("INICIANDO PROCESO DE REGISTRO TARJETA PREVIVALE " + gson.toJson(user) + " idPais: " + idPais + " idApp: "+ idApp + " idioma: "+ idioma+ " tipo: "+ tipo + " id: " + id );
			MobilecardCardResponse response = new MobilecardCardResponse();
			ResponseSMS respPrevi = new ResponseSMS();
			MobileCardCard card = new MobileCardCard();
			card.setBalance(0.0);
			card.setMovements(new ArrayList<MobilecardCardMovement>());
		 try{
			 
			 PreviValeService previvale = new PreviValeService();
			 
			 if(tipo.equalsIgnoreCase("negocio")){
				
				 /*******************************ACTUALIZANDO DATOS DE COMERCIO******************************************/
				 UpdateCommerce(user.getUsr_direccion(), user.getUsr_colonia(), user.getUsr_cp(), id, user.getUsr_id_estado(), user.getUsr_ciudad(), idApp);
				 
				 if(user.getPan()!= null && !user.getPan().isEmpty()){
					 /************REGISTRANDO COMERCIO CON TARJETA**********/
					     respPrevi = previvale.registrarConTarjeta(id, UtilsService.setSMS(AddcelCrypto.decryptHard(user.getPan())), idApp, idPais, idioma);
				 }else{
					 /*******************REGISTRAR COMERCIO SIN TARJETA************************/
						 respPrevi = previvale.registrarSinTarjeta(id, idApp, idPais, idioma);
				      }
				 /******************************SETEANDO VALORES A LA TARJETA***********************************/
				 if(respPrevi.getCode()==1000){
			 			PrevivaleStock  stok = mapper.GetPrevivaleCardStock(respPrevi.getData().getIdPrevivaleCardStock());
			 			 CardCommerce cardC = mapper.getCardCommerce(stok.getIdtarjetas_establecimiento(), idApp);
						 card.setPan( AddcelCrypto.encryptHard(UtilsService.getSMS(cardC.getNumero_tarjeta())));
						 card.setVigencia(AddcelCrypto.encryptHard(UtilsService.getSMS(cardC.getVigencia())));
						 card.setCodigo(AddcelCrypto.encryptHard(UtilsService.getSMS(cardC.getCt())));
						 card.setNombre(cardC.getNombre_tarjeta());
			 		}
			 
			 }else
				 if(tipo.equalsIgnoreCase("usuario")){
					 /*******************************ACTUALIZANDO DATOS DE USUARIO************************************/
					 UpdateUserPrevivale(id, user.getUsr_ciudad(), user.getUsr_colonia(), user.getMaterno(), user.getUsr_cp(), user.getUsr_direccion(), user.getUsr_id_estado(), idApp);
					
					 if(user.getPan()!= null && !user.getPan().isEmpty()){
						 LOGGER.debug("REGISTRANDO PREVIVALE USUARIO CON TARJETA...");	
						 respPrevi = previvale.registrarUsuarioConTarjeta(UtilsService.setSMS(AddcelCrypto.decryptHard(user.getPan())), id, idApp, idPais, idioma);
					 }else
					 {
						 LOGGER.debug("REGISTRANDO PREVIVALE USUARIO SIN TARJETA");
						 respPrevi = previvale.registrarUsuarioSinTarjeta(id, idApp, idPais, idioma);
					 }
					 
					 /*******************SETEANDO VALORES DE TARJETA Y ACTIVANDO**********************/
					 if(respPrevi.getCode()==1000){
						 MobilecardCardResponse r = getMobilecard(id, idioma, idPais, idApp);
						 card = r.getCard();
						 ActivateCardPrevivale(id, card.getIdTarjeta(), idApp, idioma);
					 }
				 }
			 
			 if(respPrevi.getCode() == 1000)
				 response.setIdError(0);
			 else
				 response.setIdError(respPrevi.getCode());
			 
			 response.setMensajeError(respPrevi.getMessage());
			 response.setCard(card);
			 
		 }catch(Exception ex){
			 LOGGER.error("ERROR AL REGISTRAR TARJETA", ex);
			 response.setIdError(2000);
			 if(idioma.equals("es"))
				 response.setMensajeError("ERROR INESPERADO, INTENTE MAS TARDE");
			 else
				 response.setMensajeError("UNEXPECTED ERROR, TRY LATER");
		 }
		 return response;
		 
	 }
	
	private void UpdateCommerce(String calle, String colonia, String cp, long id,long idEstado, String municipio, int idApp){
		
		try{
			 LOGGER.debug("******************ACTUALIZANDO DATOS COMERCIO/PREVIVALE*****************");
			 Commerce commerce = new Commerce();
			 commerce.setCalle(calle);
			 commerce.setColonia(colonia);
			 commerce.setCp(cp);
			 commerce.setId(id);
			 commerce.setId_estado(idEstado);
			 commerce.setMunicipio(municipio);
			 commerce.setId_aplicacion(idApp);
			 mapper.UpdateCommerce(commerce);
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR COMERCIO", ex);
		}
	}
	
	private void UpdateUserPrevivale(long id,String ciudad,String colonia, String materno, String cp, String direccion, int idEstado, int idApp){
		try{
			 LOGGER.debug("***************ACTUALIZANDO DATOS DE USUARIO/PREVIVALE**************");
			 User userUp = new User();
			 userUp.setId_usuario(id);
			 userUp.setUsr_ciudad(ciudad);
			 userUp.setUsr_colonia(colonia);
			 userUp.setMaterno(materno);
			 userUp.setUsr_cp(cp);
			 userUp.setUsr_direccion(direccion);
			 userUp.setUsr_id_estado(idEstado);
			 userUp.setId_aplicacion(idApp);
			 mapper.updateUserDataPrevivale(userUp); 
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACGTUALIZAR DATOS DE USUARIO PREVIVALE", ex);
		}
	}
	
	private void ActivateCardPrevivale(long id, long idTarjeta,int idApp, String idioma){
		try{
			 LOGGER.debug("*********ACTIVANDO TARJETA PREVIVALE************");
			 HashMap parameters = new HashMap<>();
			 parameters.put("nusuario", id);
			 parameters.put("idapp",idApp );
			 parameters.put("nnumtarjeta", "");
			 parameters.put("nnumtarjetacrip", "");
			 parameters.put("nvigencia", "");
			 parameters.put("nproceso", 2);
			 parameters.put("nct", "");
			 parameters.put("ntarjeta", "");
			 parameters.put("domamex","");
			 parameters.put("cpamex", "");
			 parameters.put("idioma", idioma);
			 parameters.put("mobilecard", 0);
			 parameters.put("tipotarjeta", "");
			 parameters.put("nidtarjetausuario", idTarjeta);
			 McResponse mcResponse = active(parameters);
			 LOGGER.debug("Repuesta activacion walllet: " + gson.toJson(mcResponse));
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTUALIZAR DATOS USUARIO/PREVIVALE", ex);
		}
	}
	
	
	public TarjetaResponse activateDefaultCard(int idApp, int idPais, String idioma, DefaultCardReq req){
		TarjetaResponse response = new TarjetaResponse();
		HashMap parameters = new HashMap<>();
		try{
			
			parameters.put("nusuario", req.getIdUser());
			parameters.put("idapp",idApp );
			parameters.put("nnumtarjeta", "");
			parameters.put("nnumtarjetacrip", "");
			parameters.put("nvigencia", "");
			parameters.put("nproceso", 2);
			parameters.put("nidtarjetausuario", req.getIdCard());
			parameters.put("nct", "");
			parameters.put("ntarjeta", "");
			parameters.put("domamex","");
			parameters.put("cpamex", "");
			parameters.put("idioma", idioma);
			parameters.put("mobilecard", 0);
			parameters.put("tipotarjeta","" );
			
			LOGGER.debug("ACTIVANDO TARJETA {} ", req.getIdCard());
			McResponse mcResponse = active(parameters);
			
			LOGGER.debug("OBTENIENDO LISTA DE TARJETAS DESPUES DE ACTIVAR DEFAULT");
			response = WithoutMovement(req.getIdUser(), idioma, idApp, 6, 0); //tarjetas sin movimientos
			
			if(mcResponse.getIdError()!= 0){
				response.setIdError(mcResponse.getIdError());
				response.setMensajeError(mcResponse.getMensajeError());
			}
			
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ACTICAR TARJETA",ex);
			if(idioma.equals("es"))
				response.setMensajeError("Error al activar tarjeta como predeterminada, intente mas tarde");
			else
				response.setMensajeError("Error activating card as default, try later");
		}
		return response;
		
	}
	
	/*public TransactionsRes getTransactionPerMonth(long idUsuario, int idApp, int idPais, String idioma){
		TransactionsRes transactions = new TransactionsRes();
		String pan = "";
		String franquicia = "";
		String aux = "";
		TransactionData data = null;
		Transactions t = null;
		try{
			LOGGER.debug("/**********OBTENIENDO TRANSACCIONES {}************",idUsuario);
			String jsonDB = mapper.getTransactionPerMonth(idUsuario);
			transactions = gson.fromJson(jsonDB, TransactionsRes.class);
			int tam = transactions.getData().size();
			int tamt = 0;
			/////********Iterando meses**********
			for(int i = 0; i<tam; i++){
				
				data = transactions.getData().get(i);
				tamt = data.getTransacciones().size();
				
				/**********iterando lista de transacciones************
				for(int j = 0; j< tamt; j++){
					
					t = data.getTransacciones().get(j);
					pan = (t.getTarjeta() == null || t.getTarjeta().isEmpty()) ? "" : UtilsService.getSMS(t.getTarjeta());
					aux = pan;
					pan = UtilsMC.maskCard(pan, "*");
					/******seteando tarjeta enmascarada******
					transactions.getData().get(i).getTransacciones().get(j).setTarjeta(pan);
					/*******************seteando url de la imagen**********************
					if(!aux.isEmpty()){
						CardImage cardImage = mapper.getCardImage(aux.substring(0, 6));
						if(cardImage != null){
							transactions.getData().get(i).getTransacciones().get(j).setImgFull(cardImage.getUrl_imagen_larga());
						}else{
							franquicia = mapper.getNameFranquicia(aux);
							if(franquicia!= null && franquicia.equals("VISA")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Visa.png");
							}else if(franquicia!= null && franquicia.equals("MasterCard")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Mastercard.png");
							}else if(franquicia!= null && franquicia.equals("AmEx")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/completas/Amex.png");
							}else {
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Mobilecard.png");
							}
						}
					}
				}
				
			}
			
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TRANSACCIONES " + idUsuario, ex);
		}
		return transactions;
	}*/
	
	public TransactionsRes getTransactionPerMonth(long idUsuario, int idApp, int idPais, String idioma){
		TransactionsRes transactions = new TransactionsRes();
		String pan = "";
		String franquicia = "";
		String aux = "";
		TransactionData data = null;
		Transactions t = null;
		TarjetaResponse lcard = new TarjetaResponse();
		
		
		HashMap parameters = new HashMap<>();
		parameters.put("nusuario", idUsuario);
		parameters.put("idapp",idApp );
		parameters.put("idioma", idioma);
		parameters.put("nproceso", 8);
		parameters.put("nnumtarjetacrip", "");
		parameters.put("nnumtarjeta", "");
		parameters.put("nvigencia", "");
		parameters.put("nidtarjetausuario", 0);
		parameters.put("nct", "");
		parameters.put("ntarjeta", "");
		parameters.put("domamex","" );
		parameters.put("cpamex", "");
		parameters.put("mobilecard", 0);
		parameters.put("tipotarjeta", "");
		
		try{
			LOGGER.debug("/**********OBTENIENDO TRANSACCIONES {}************/",idUsuario);
			String jsonDB = mapper.getTransactionPerMonth(idUsuario, idApp, idPais, idioma);
			//LOGGER.debug("Separador "+System.getProperty("line.separator"));
			//LOGGER.debug("split de 0" + jsonDB.split("\\*\\*")[0]);
			//LOGGER.debug("split de 1" + jsonDB.split("\r\n")[0]);
			//LOGGER.debug("JSON 1" + jsonDB.replaceAll("\r\n", "").replaceAll("\r","").replaceAll("\n", ""));
			LOGGER.debug("JSON: " + jsonDB.replaceAll("\\*\\*","") );
			transactions = gson.fromJson(jsonDB.replaceAll("\\*\\*",""), TransactionsRes.class);
			int tam = transactions.getData().size();
			int tamt = 0;
			/////********Iterando meses**********//
			for(int i = 0; i<tam; i++){
				
				data = transactions.getData().get(i);
				tamt = data.getTransacciones().size();
				
				/**********iterando lista de transacciones************/
				for(int j = 0; j< tamt; j++){
					
					t = data.getTransacciones().get(j);
					pan = (t.getTarjeta() == null || t.getTarjeta().isEmpty()) ? "" : t.getTarjeta();
					//aux = pan;
					//pan = UtilsMC.maskCard(pan, "*");
					/******seteando tarjeta enmascarada******/
					if(!pan.isEmpty()){
						parameters.put("nnumtarjetacrip", pan);
						lcard = getCards(parameters, idUsuario, idApp, idPais, idioma);
						
						if(lcard != null && lcard.getTarjetas().size() > 0 ){
							LOGGER.debug("Tarjeta: " + gson.toJson(lcard.getTarjetas().get(0)));
							transactions.getData().get(i).getTransacciones().get(j).setCard(lcard.getTarjetas().get(0));
						}else
							LOGGER.debug("****NO SE ENCONTRO TARJETA****");
					}
					removeNull(i, j, transactions);
					//transactions.getData().get(i).getTransacciones().get(j).setTarjeta(pan);
					/*******************seteando url de la imagen**********************/
					/*if(!aux.isEmpty()){
						CardImage cardImage = mapper.getCardImage(aux.substring(0, 6));
						if(cardImage != null){
							transactions.getData().get(i).getTransacciones().get(j).setImgFull(cardImage.getUrl_imagen_larga());
						}else{
							franquicia = mapper.getNameFranquicia(aux);
							if(franquicia!= null && franquicia.equals("VISA")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Visa.png");
							}else if(franquicia!= null && franquicia.equals("MasterCard")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Mastercard.png");
							}else if(franquicia!= null && franquicia.equals("AmEx")){
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/completas/Amex.png");
							}else {
								transactions.getData().get(i).getTransacciones().get(j).setImgFull("http://199.231.160.203/images/cards/full/Mobilecard.png");
							}
						}
					}*/
				}
				
			}
			
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL OBTENER TRANSACCIONES " + idUsuario, ex);
		}
		return transactions;
	}
	
	private void removeNull(int i, int j,TransactionsRes transactions){
		
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getDate() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setDate("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getMovements() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setMovements(new ArrayList<MobilecardCardMovement>());
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getNombre() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setNombre("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getPan() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setPan("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getTipo() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setTipo("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getVigencia() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setVigencia("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getCodigo() == null )
			transactions.getData().get(i).getTransacciones().get(j).getCard().setCodigo("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getDomAmex() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setDomAmex("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getCpAmex() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setCpAmex("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getTipoTarjeta() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setTipoTarjeta("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getPhoneNumberActivation() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setPhoneNumberActivation("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getClabe() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setClabe("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getImg_full() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setImg_full("");
		if(transactions.getData().get(i).getTransacciones().get(j).getCard().getImg_short() == null)
			transactions.getData().get(i).getTransacciones().get(j).getCard().setImg_short("");
	}
	
	public boolean isActive(Integer idApp){
		boolean active = true;
		String band = null;
		
		band = mapper.isAppActive(idApp);
		if(band != null)
			if(band.equals("T"))
				active = true;
			else
				active = false;
		
		if(active)
			LOGGER.debug("SERVICIO AUTORIZADO PARA ID {}", idApp);
		else
			LOGGER.debug("SERVICIO DENEGADO PARA ID {}", idApp);
		return active;
	}
	
	

}
