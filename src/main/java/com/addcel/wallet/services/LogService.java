package com.addcel.wallet.services;

import java.util.Arrays;
import java.util.List;

import com.addcel.wallet.client.model.vo.TarjetaResponse;

public class LogService {
	
	private static final List AddError = Arrays.asList("Error al registrar tarjeta","Error Registering Card");
	
	private static final List getCards = Arrays.asList("error al obtener tarjetas","Error Getting Cards");
	
	private static final List CardDelete = Arrays.asList("error al eliminar tarjeta","Error Removing Card");
	
	private static final List CardUpdate = Arrays.asList("error al actualizar tarjeta","Error Updating Card");
	
	private static final List FirstViewError = Arrays.asList("Error al crear Tarjeta en FirstView","Error creating Card in First View");
	
	private static final List AddMobileCard = Arrays.asList("Error al obtener tarjeta Mobilecard","Error Getting Mobilecard Card");
	
	
	public static TarjetaResponse getAddMessageError(int idioma){
		TarjetaResponse response = new TarjetaResponse();
		 response.setIdError(100);
		 response.setMensajeError((String)AddError.get(idioma-1));
		// response.setTarjetas(new List<MobileCardCard>());
		return response;
	}
	
	public static TarjetaResponse getCardsMessageerror(int idioma){
		TarjetaResponse response = new TarjetaResponse();
		 response.setIdError(102);
		 response.setMensajeError((String)getCards.get(idioma-1));
		return response;
	}
	
	public static TarjetaResponse CardDeleteMessageError(int idioma){
		TarjetaResponse response = new TarjetaResponse();
		 response.setIdError(103);
		 response.setMensajeError((String)CardDelete.get(idioma-1));
		return response;
	}
	
	public static TarjetaResponse CardUpdateMessageError(int idioma){
		TarjetaResponse response = new TarjetaResponse();
		 response.setIdError(101);
		 response.setMensajeError((String)CardUpdate.get(idioma-1));
		return response;
	}
	
	public static TarjetaResponse ErrorFV(int idioma){
		TarjetaResponse response =  new TarjetaResponse();
		response.setIdError(104);
		response.setMensajeError((String)FirstViewError.get(idioma-1));
		return response;
	}
	
	public static TarjetaResponse AddMobileCardError(int idioma){
		TarjetaResponse response =  new TarjetaResponse();
		response.setIdError(104);
		response.setMensajeError((String)AddMobileCard.get(idioma-1));
		return response;
	}
	
	
}
