package com.addcel.wallet.client.model.vo;

public class DefaultCardReq {

	private long idUser;
	private long idCard;

	public DefaultCardReq() {
		// TODO Auto-generated constructor stub
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public long getIdCard() {
		return idCard;
	}

	public void setIdCard(long idCard) {
		this.idCard = idCard;
	}

}
