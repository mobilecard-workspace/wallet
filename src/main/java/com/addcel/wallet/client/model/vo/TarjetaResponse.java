package com.addcel.wallet.client.model.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TarjetaResponse extends McResponse {

	private boolean hasMobilecard;
	
	@SerializedName("MobileCardCard")
	@Expose
	private List<MobileCardCard>	tarjetas;

	public TarjetaResponse() {
	}
	
	public List<MobileCardCard> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(List<MobileCardCard> tarjetas) {
		this.tarjetas = tarjetas;
	}

	public boolean isHasMobilecard() {
		return hasMobilecard;
	}

	public void setHasMobilecard(boolean hasMobilecard) {
		this.hasMobilecard = hasMobilecard;
	}
	
	
	
	
}
