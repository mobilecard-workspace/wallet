package com.addcel.wallet.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseSMS {

	private int code;
	
	private String message;
	
	private PreviValeData data;
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setData(PreviValeData data) {
		this.data = data;
	}
	
	public PreviValeData getData() {
		return data;
	}
	
}