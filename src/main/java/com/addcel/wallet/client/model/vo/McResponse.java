package com.addcel.wallet.client.model.vo;

public class McResponse {
	
	private int idError ;
	private String mensajeError;
	
	public McResponse(int idError,String mensajeError) {
		this.idError = idError;
		this.mensajeError = mensajeError;
	}
	
	public McResponse() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	

}
