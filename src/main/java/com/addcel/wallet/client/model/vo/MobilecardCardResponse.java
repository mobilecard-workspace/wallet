package com.addcel.wallet.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MobilecardCardResponse extends McResponse {
	
	private MobileCardCard card;
	
	public MobilecardCardResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public MobileCardCard getCard() {
		return card;
	}
	
	public void setCard(MobileCardCard card) {
		this.card = card;
	}
}
