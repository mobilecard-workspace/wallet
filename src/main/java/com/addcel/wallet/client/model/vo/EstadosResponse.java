package com.addcel.wallet.client.model.vo;

import java.util.List;
import com.addcel.wallet.mybatis.model.vo.Estados;

public class EstadosResponse extends McResponse {

	List<Estados> estados;
	
	public EstadosResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Estados> getEstados() {
		return estados;
	}

	public void setEstados(List<Estados> estados) {
		this.estados = estados;
	}

	
	
	
}
