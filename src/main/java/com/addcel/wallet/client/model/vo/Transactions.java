package com.addcel.wallet.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transactions {
	
	private long id;
	
	private String date;
	
	private String ticket;
	
	private Double total;
	
	private Double comision;
	
	private Double importe;
	
	private Integer status;
	
	@JsonIgnore
	private String tarjeta;
	
	private MobileCardCard card;
	
	@JsonIgnore
	@SerializedName("img_full")
	@Expose
	private String imgFull;
	
	public Transactions() {
		// TODO Auto-generated constructor stub
		this.card = new MobileCardCard();
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getComision() {
		return comision;
	}

	public void setComision(Double comision) {
		this.comision = comision;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getImgFull() {
		return imgFull;
	}

	public void setImgFull(String imgFull) {
		this.imgFull = imgFull;
	}
	
	public void setCard(MobileCardCard card) {
		this.card = card;
	}
	
	public MobileCardCard getCard() {
		return card;
	}

}
