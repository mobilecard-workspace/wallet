package com.addcel.wallet.client.model.vo;

import java.util.List;

public class TransactionData {

	private String mes;
	private List<Transactions> transacciones = null;
	
	public TransactionData() {
		// TODO Auto-generated constructor stub
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public List<Transactions> getTransacciones() {
		return transacciones;
	}

	public void setTransacciones(List<Transactions> transacciones) {
		this.transacciones = transacciones;
	}
	
	
}
