package com.addcel.wallet.client.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class previvaleUser {

	private String usr_nombre;
	private String usr_ciudad;
	private String usr_colonia;
	private String curp;
	private Integer id_aplicacion;
	private Long id_usuario;
	private String pan;
	private String rfc;
	private String usr_cp;
	private String usr_direccion;
	private Integer usr_id_estado;
	private String materno;
	private String paterno;
	
	
	
	public previvaleUser() {
		// TODO Auto-generated constructor stub
	}



	public String getUsr_nombre() {
		return usr_nombre;
	}



	public void setUsr_nombre(String usr_nombre) {
		this.usr_nombre = usr_nombre;
	}



	public String getUsr_ciudad() {
		return usr_ciudad;
	}



	public void setUsr_ciudad(String usr_ciudad) {
		this.usr_ciudad = usr_ciudad;
	}



	public String getUsr_colonia() {
		return usr_colonia;
	}



	public void setUsr_colonia(String usr_colonia) {
		this.usr_colonia = usr_colonia;
	}



	public String getCurp() {
		return curp;
	}



	public void setCurp(String curp) {
		this.curp = curp;
	}



	public Integer getId_aplicacion() {
		return id_aplicacion;
	}



	public void setId_aplicacion(Integer id_aplicacion) {
		this.id_aplicacion = id_aplicacion;
	}



	public Long getId_usuario() {
		return id_usuario;
	}



	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}



	public String getPan() {
		return pan;
	}



	public void setPan(String pan) {
		this.pan = pan;
	}



	public String getRfc() {
		return rfc;
	}



	public void setRfc(String rfc) {
		this.rfc = rfc;
	}



	public String getUsr_cp() {
		return usr_cp;
	}



	public void setUsr_cp(String usr_cp) {
		this.usr_cp = usr_cp;
	}



	public String getUsr_direccion() {
		return usr_direccion;
	}



	public void setUsr_direccion(String usr_direccion) {
		this.usr_direccion = usr_direccion;
	}



	public Integer getUsr_id_estado() {
		return usr_id_estado;
	}



	public void setUsr_id_estado(Integer usr_id_estado) {
		this.usr_id_estado = usr_id_estado;
	}



	public String getMaterno() {
		return materno;
	}



	public void setMaterno(String materno) {
		this.materno = materno;
	}



	public String getPaterno() {
		return paterno;
	}



	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	
	
}
