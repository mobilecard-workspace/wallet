package com.addcel.wallet.client.model.vo;

import java.util.List;

public class TransactionsRes extends McResponse {

	private List<TransactionData> data = null;
	
	public TransactionsRes() {
		// TODO Auto-generated constructor stub
	}
	
	public void setData(List<TransactionData> data) {
		this.data = data;
	}
	
	public List<TransactionData> getData() {
		return data;
	}
	
}
