package com.addcel.wallet.client.viamericas;

public class ProfileCardRequest {

	private String idSender;
	private String paymentType;
	private String cardNumber;
	private String expDate;
	private String cvv;
	private String cardFirstName;
	private String cardLastName;
	private String nickName;
	private long idUsuario;
	private String address;
	
	public ProfileCardRequest() {
		// TODO Auto-generated constructor stub
	}
	
	public String getIdSender() {
		return idSender;
	}
	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getCardFirstName() {
		return cardFirstName;
	}
	public void setCardFirstName(String cardFirstName) {
		this.cardFirstName = cardFirstName;
	}
	public String getCardLastName() {
		return cardLastName;
	}
	public void setCardLastName(String cardLastName) {
		this.cardLastName = cardLastName;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
