package com.addcel.wallet.client.viamericas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.ingo.bridge.client.model.vo.ServicesEndPoint;
import com.google.gson.Gson;

@Service
public class ViamericasClient {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ViamericasClient.class);
	private Gson gson = new Gson();
	
	public ProfileCardResponse createSenderProfileCard(ProfileCardRequest profileCard){
		
		ProfileCardResponse responseProfile = new ProfileCardResponse();
		try{
			
			LOGGER.debug("[INICIANDO PROCESO createSenderProfileCard ]");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", gson.toJson(profileCard));
			
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			RestTemplate restTemplate = new RestTemplate();
			LOGGER.debug("[ENVIANDO PETICION ] ["+profileCard.getIdUsuario()+"] " + map.get("json"));
			ResponseEntity<String> responseJson = restTemplate.postForEntity(ServicesEndPoint.ViamericascreateSenderProfileCard, map,String.class);
			responseProfile = gson.fromJson(responseJson.getBody(), ProfileCardResponse.class);
			LOGGER.debug("[RESPUESTA VIAMERICAS] ["+profileCard.getIdUsuario()+"] " + responseJson.getBody());
			
		}catch(Exception ex){
			LOGGER.error("[ERROR AL ENVIAR PETICION VIAMERICAS ]["+profileCard.getIdUsuario()+"] " , ex);
			responseProfile.setHashCodeCalc(false);
			responseProfile.setIdError(-1);
			responseProfile.setMensajeError("An error occurred, please try again.");
		}
		
		return responseProfile;
	}

}
