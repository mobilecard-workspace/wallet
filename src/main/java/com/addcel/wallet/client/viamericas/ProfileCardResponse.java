package com.addcel.wallet.client.viamericas;

public class ProfileCardResponse {

	private boolean hashCodeCalc;
	private int idError;
	private String mensajeError;
	
	public ProfileCardResponse() {
		// TODO Auto-generated constructor stub
	}

	public boolean isHashCodeCalc() {
		return hashCodeCalc;
	}

	public void setHashCodeCalc(boolean hashCodeCalc) {
		this.hashCodeCalc = hashCodeCalc;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	
	
}
